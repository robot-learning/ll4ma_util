import sys

import torch
import torch.nn as nn
import torch.nn.functional as F

from typing import Optional, List
from dataclasses import dataclass

from ll4ma_util import torch_util
from ll4ma_util.torch_distributions import RotMVN, ExtendedRotMVN, RotMixtureModel

def get_activation(activation):
    """
    Returns specified activation function. Note this is preferable to functional 
    interface since these can be added to ModuleList.
    """
    if activation == 'relu':
        return nn.ReLU()
    elif activation == 'leaky_relu':
        return nn.LeakyReLU(0.2, inplace=True)
    elif activation == 'prelu':
        return nn.PReLU()
    elif activation == 'sigmoid':
        return nn.Sigmoid()
    elif activation == 'tanh':
        return nn.Tanh()
    elif activation == 'softplus':
        return nn.Softplus()
    else:
        raise ValueError(f"Unknown activation type: {activation}")


def get_norm(norm, out_size):
    if norm == 'layer2d':
        # Using 1 group is equivalent to layer norm, but seems you need
        # to do it this way with groupnorm for convolution layers
        return nn.GroupNorm(1, out_size)
    elif norm == 'layer':
        return nn.LayerNorm((out_size,))
    elif norm == 'batch':
        return nn.BatchNorm2d(num_features=out_size)
    else:
        raise ValueError(f"Unknown normalization type: {norm}")

    
class LinearBlock(nn.Module):

    def __init__(self, in_size, out_size, activation=None, norm=None):
        super().__init__()

        self.layers = nn.ModuleList()
        self.layers.append(nn.Linear(in_size, out_size))
        if activation:
            self.layers.append(get_activation(activation))
        if norm is not None:
            self.layers.append(get_norm(norm, out_size))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x
    

class Conv2dBlock(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0,
                 activation=None, norm=None):
        super().__init__()

        self.layers = nn.ModuleList()
        self.layers.append(nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding))
        if activation:
            self.layers.append(get_activation(activation))
        if norm is not None:
            self.layers.append(get_norm(norm, out_channels))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


class ConvTranspose2dBlock(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0,
                 activation=None, norm=None):
        super().__init__()

        self.layers = nn.ModuleList()
        self.layers.append(nn.ConvTranspose2d(in_channels, out_channels, kernel_size, stride, padding))
        if activation:
            self.layers.append(get_activation(activation))
        if norm is not None:
            self.layers.append(get_norm(norm, out_channels))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

    
class MLP(nn.Module):

    def __init__(self, in_size, out_size, hidden_sizes=[], activation='relu',
                 norm=None, out_activation=None, out_norm=None, vae=False):
        """
        Multi-layer perception module. Stacks linear layers with customizable sizes, 
        activations, and normalization.

        Args:
            in_size (int): Size of input tensor
            out_size (int): Size of output tensor
            hidden_sizes (List[int]): Output sizes of each hidden layer
            activation (str): Activations to apply to each hidden layer
            layer_norms (bool): Apply layer norm to hidden layers if true
            out_activation (str): Activation to apply to output layer (default is none)
            out_layer_norm (bool): Apply layer norm to output layer if true
            vae (bool): Sample output as VAE
        Returns:
            Output of last layer; if vae=True, returns sample of output as well as mean and std

        TODO right now only layer norm supported, can make more general
        """
        super().__init__()

        self.vae = vae
        if self.vae:
            out_size *= 2
        
        self.layers = nn.ModuleList()
        prev_size = in_size
        for size in hidden_sizes:
            self.layers.append(LinearBlock(prev_size, size, activation, norm))
            prev_size = size
        self.layers.append(LinearBlock(prev_size, out_size, out_activation, out_norm))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)

        if self.vae:
            mu, log_std = torch.chunk(x, 2, dim=-1)
            std = torch.exp(log_std)
            noise = torch.randn_like(mu)
            if self.training:
                return mu + std * noise
            else:
                return mu
        else:
            return x

    
class ImageEncoder(nn.Module):
  
    def __init__(self, img_channels, embedding_size, out_channels=[], kernel_sizes=[],
                 strides=[], paddings=[], activation='relu', norm=None):
        super().__init__()

        if (len(out_channels) != len(kernel_sizes) or len(out_channels) != len(strides) or
            len(out_channels) != len(paddings)):
            raise ValueError("Must specify out_channels, kernel_sizes, strides, and "
                             "paddings for every conv layer")

        self.convs = nn.ModuleList()
        prev_c = img_channels
        for c, k, s, p in zip(out_channels, kernel_sizes, strides, paddings):
            self.convs.append(Conv2dBlock(prev_c, c, k, s, p, activation, norm))
            prev_c = c

        # TODO ideally infer input size from CNN outputs
        self.linears = nn.ModuleList([LinearBlock(1024, embedding_size)])
        
    def forward(self, x):
        batch_size = x.size(0)
        
        for conv in self.convs:
            x = conv(x)
            
        # x = x.view(batch_size, -1)
        x = x.reshape(batch_size, -1)
        for linear in self.linears:
            x = linear(x)
        
        return x
  

class ImageDecoder(nn.Module):
  
    def __init__(self, img_channels, in_size, embedding_size, out_channels=[],
                 kernel_sizes=[], strides=[], paddings=[], activation='relu',
                 out_activation='sigmoid', norm=None):
        super().__init__()

        self.embedding_size = embedding_size

        self.linears = nn.ModuleList()
        if in_size != embedding_size:
            self.linears.append(LinearBlock(in_size, embedding_size, activation))

        self.convs = nn.ModuleList()
        prev_c = embedding_size
        for c, k, s, p in zip(out_channels, kernel_sizes[:-1], strides[:-1], paddings[:-1]):
            self.convs.append(ConvTranspose2dBlock(prev_c, c, k, s, p, activation, norm))
            prev_c = c
        self.convs.append(ConvTranspose2dBlock(out_channels[-1], img_channels, kernel_sizes[-1],
                                               strides[-1], paddings[-1], out_activation))
        
    def forward(self, x):
        for linear in self.linears:
            x = linear(x)

        x = x.view(-1, self.embedding_size, 1, 1)
        for conv in self.convs:
            x = conv(x)
        
        return x


class Ortho6dModel(nn.Module):
    """
    Rotation model based on [1] that encodes rotation matrix to a 6-D
    representation to avoid discontinuities in representation learning.

    TODO if you want this to be an encoder/decoder structure for incorporating
    in a multisensory space you'll need to rethink this. I think you have to
    do this constraint even to encode, i.e. you'll supervise the representation
    with the target pose before integrating with the representation space, and
    if you're decoding I think you do a similar thing, in that case the input
    size would vary since it's coming from a latent vector instead of 9-D rot.

    [1] Zhou, Yi, et al. "On the continuity of rotation representations in 
    neural networks." CVPR 2019
    """
    
    def __init__(self, hidden_sizes=[128, 128], activation='relu', norm=None):
        super().__init__()

        self.mlp = MLP(9, 6, hidden_sizes, activation, norm)

    def forward(self, x):
        out_rotation_raw = self.mlp(x)
        out_rotation_matrix = torch_util.ortho6d_to_rotation(out_rotation_raw)
        return out_rotation_raw, out_rotation_matrix
    

class MDN(nn.Module):
    """
    Mixture Density Network (MDN).

    This is based on Fabio's implementation. The main difference is the covariance
    computation which is taken from [1] to ensure positive semi-definiteness
    without limiting too much the values the covariance matrix can take.

    [1] Lutter, Michael, Christian Ritter, and Jan Peters; "Deep lagrangian 
        networks: Using physics as model prior for deep learning"; ICLR 2019
    """
    
    def __init__(self, in_size, out_size, hidden_sizes=[64, 64], n_components=2,
                 activation='relu', norm=None, diag_only=True, diag_scalar=1e-8):
        """
        """
        super().__init__()
        
        self.out_size = out_size
        self.n_components = n_components
        self.diag_scalar = diag_scalar

        last_size = hidden_sizes[-1]
        self.mlp = MLP(in_size, last_size, hidden_sizes, activation, norm)
        self.alpha = LinearBlock(last_size, n_components)
        self.mu = LinearBlock(last_size, n_components * out_size)
        # Need to use a non-negative activation (e.g. relu, softplus) to ensure
        # positive semi-definite. It's okay if it outputs zero elements since
        # we add a small scalar to be sure. Empirically, softplus seems to be
        # working better than relu
        self.L_diag = LinearBlock(last_size, n_components * out_size, 'softplus')
        if out_size > 1 and not diag_only:
            self.L = LinearBlock(last_size, int(0.5 * n_components * out_size * (out_size - 1)))
        else:
            self.L = None

    def forward(self, x):
        x = self.mlp(x)  # (b, last_size)
        alpha = torch.softmax(self.alpha(x), -1)

        # # TODO temp
        # alpha = torch.full_like(alpha, 1./self.n_components)

        
        mu = self.mu(x).reshape(-1, self.out_size, self.n_components)
        L_diag = self.L_diag(x).reshape(-1, self.out_size, self.n_components)
        L_diag = torch.add(L_diag, self.diag_scalar) # Ensure positive semi-definite
        L = None
        if self.L is not None:
            L = self.L(x).reshape(-1, int(0.5 * self.out_size * (self.out_size - 1)), self.n_components)
        sigma_tril = self.get_sigma_tril(L_diag, L)
        return alpha, mu, sigma_tril

    def get_sigma_tril(self, L_diag, L):
        device = L_diag.device
        dtype = L_diag.dtype
        batch_size, out_size, n_components = L_diag.size()

        tril_idxs = torch.tril_indices(out_size, out_size, offset=-1)
        diag_idxs = torch.arange(out_size).repeat(2, 1)

        tril = torch.zeros(batch_size, out_size, out_size, n_components, dtype=dtype).to(device)
        tril[:, diag_idxs[0], diag_idxs[1], :] = L_diag[:,:,:]
        if L is not None:
            tril[:, tril_idxs[0], tril_idxs[1], :] = L[:,:,:]
        return tril


class PoseMDN(MDN):
    """
    Mixture Density Network (MDN) for poses (3 - dof position, 6 - orientation).

    Updated MDN architecture for continuous distributions on SE(3)

    """
    
    def __init__(self, in_size, out_size, num_rot_outs=1, hidden_sizes=[64, 64], n_components=2,
                 activation='relu', norm=None, diag_only=True, diag_scalar=1e-8):
        """
        """
        super().__init__(in_size, out_size+num_rot_outs*6,
                         hidden_sizes=hidden_sizes,
                         n_components=n_components,
                         activation=activation,
                         norm=norm,
                         diag_only=diag_only,
                         diag_scalar=diag_scalar)
        
        self.num_rots = num_rot_outs
        last_size = hidden_sizes[-1]
        self.cov_size = self.out_size - 3 * self.num_rots # Assuming ortho6d for orientation
        # Rotation means in ortho6d, but cov in angular velocities (3d).

        # Need to use a non-negative activation (e.g. relu, softplus) to ensure
        # positive semi-definite. It's okay if it outputs zero elements since
        # we add a small scalar to be sure. Empirically, softplus seems to be
        # working better than relu
        self.L_diag = LinearBlock(last_size, n_components * self.cov_size, 'softplus')
        if self.cov_size > 1 and not diag_only:
            self.L = LinearBlock(last_size,
                                 int(0.5 * n_components * self.cov_size * (self.cov_size - 1)))
        else:
            self.L = None

    def forward(self, x):
        x = self.mlp(x)  # (b, last_size)
        alpha = torch.softmax(self.alpha(x), -1)

        # # TODO temp
        # alpha = torch.full_like(alpha, 1./self.n_components)

        
        mu = self.mu(x).reshape(-1, self.out_size, self.n_components)
        L_diag = self.L_diag(x).reshape(-1, self.cov_size, self.n_components)
        L_diag = torch.add(L_diag, self.diag_scalar) # Ensure positive semi-definite
        L = None
        if self.L is not None:
            L = self.L(x).reshape(-1, int(0.5 * self.cov_size * (self.cov_size - 1)), self.n_components)
        sigma_tril = self.get_sigma_tril(L_diag, L)
        return alpha, mu, sigma_tril

    def get_sigma_tril(self, L_diag, L):
        device = L_diag.device
        dtype = L_diag.dtype
        batch_size, out_size, n_components = L_diag.size()

        tril_idxs = torch.tril_indices(out_size, out_size, offset=-1)
        diag_idxs = torch.arange(out_size).repeat(2, 1)

        tril = torch.zeros(batch_size, out_size, out_size, n_components, dtype=dtype).to(device)
        tril[:, diag_idxs[0], diag_idxs[1], :] = L_diag[:,:,:]
        if L is not None:
            tril[:, tril_idxs[0], tril_idxs[1], :] = L[:,:,:]
        return tril


    def make_distributions(self, alpha, mu, sigma_tril):
        device = alpha.device
        dtype = alpha.dtype
        batch_size, n_components = alpha.size()
        dists = [ExtendedRotMVN(mu[..., i], scale_tril=sigma_tril[..., i],
                                num_rotations=self.num_rots) for i in range(self.n_components)]
        return RotMixtureModel(alpha, dists)
        
        
    def loss(targets, alpha, mu, sigma_tril, is_goal=None):
        """
        Loss function for the Pose MDN network.

        Splits the inputs and target into position and orientation components.
        assuming (3 + 6) structure for position + orientation, MVN for positions are
        built which operates in euclidean speace and RotMVN for orientaions based on
        SO(3) reimannian manifolds. And joint probs is computed by addition in logspace.
        
        Note: This assumption breaks if covariances between positions and orientations
        are involved, in that case a single distribution on SE(3) manifold is need.
        """
        device = alpha.device
        dtype = alpha.dtype
        batch_size, n_components = alpha.size()
        
        result = torch.zeros(batch_size, n_components, dtype=dtype).to(device)

        e_size = self.out_size - 6 * self.num_rots
        
        euclidean_mu = mu[:, :e_size]
        euclidean_scale_tril = sigma_tril[:, :e_size, :e_size]
        euclidean_targets = targets[:, :e_size]

        rot_mu = []
        rot_scale_tril = []
        rot_targets = []
        for i in range(self.num_rots):
            st_id = e_size + i * 6
            end_id = e_size + (i + 1) * 6
            cst_id = e_size + i * 3
            cend_id = e_size + (i + 1) * 3
            rot_mu.append(mu[:, st_id:end_id])
            rot_scale_tril.append(sigma_tril[:, cst_id:cend_id, cst_id:cend_id])
            rot_targets.append(torch_util.ortho6d_to_rotation(targets[:, st_id:end_id]))

        for idx in range(n_components):
            rot_mvns = []
            mvn = D.MultivariateNormal(loc=position_mu[..., idx],
                                       scale_tril=position_scale_tril[..., idx])
            result[:,idx] = mvn.log_prob(position_targets.to(dtype)) + alpha[:,idx].log()
            for i in range(self.num_rots):
                rot_mvn = RotMVN(loc=torch_util.ortho6d_to_rotation(rot_mu[i][...,idx]),
                             scale_tril=rot_scale_tril[i][..., idx])
                result[:, idx] += rot_mvn.log_prob(rot_targets[i].to(dtype))

            if is_goal is not None:
                result[:,idx] = is_goal * result[:,idx]
        
        return -torch.mean(torch.logsumexp(result, dim=1))



@dataclass
class _RSSMReturn:
    beliefs: torch.Tensor = None
    prior_states: torch.Tensor = None
    prior_means: torch.Tensor = None
    prior_std_devs: torch.Tensor = None
    posterior_states: torch.Tensor = None
    posterior_means: torch.Tensor = None
    posterior_std_devs: torch.Tensor = None


class RSSM(nn.Module):
    """
    This is the recurrent state-space model (RSSM) from PlaNet. This code is adapted directly from
    the pytorch port of PlaNet: https://github.com/Kaixhin/PlaNet/blob/master/models.py#L15
    """
    __constants__ = ['min_std_dev']

    def __init__(self, belief_size, state_size, action_size, hidden_size,
                 embedding_size, activation='relu', min_std_dev=0.1):
        """
        belief_size: Size of RNN hidden state
        state_size: Size of latent state (you decode from posterior of this state)
        action_size: Size of combined action vector (probably concatenated encodes)
        hidden_size: Intermediate size between between combined embedding+belief in posterior
                     (or just belief in prior) and 2*state_size for distribution params. 
        embedding_size: Size of multisensory embedding (from multisensory encoder)
        """
        super().__init__()
        self.act_fn = get_activation(activation)
        self.min_std_dev = min_std_dev
        self.fc_embed_state_action = nn.Linear(state_size + action_size, belief_size)
        self.rnn = nn.GRUCell(belief_size, belief_size)
        self.fc_embed_belief_prior = nn.Linear(belief_size, hidden_size)
        self.fc_state_prior = nn.Linear(hidden_size, 2 * state_size) # Output mean and std dev
        self.fc_embed_belief_posterior = nn.Linear(belief_size + embedding_size, hidden_size)
        self.fc_state_posterior = nn.Linear(hidden_size, 2 * state_size) # Output mean and std dev
        
    def forward(self, prev_state, actions, prev_belief, observations=None):
        """
        Operates over (previous) state, (previous) actions, (previous) belief,
        and (current) encoded observation. Diagram of expected inputs and outputs for 
        T = 5 (-x- signifying beginning of output belief/state that gets sliced off):
            t :  0  1  2  3  4  5
            o :    -X--X--X--X--X-
            a : -X--X--X--X--X-
            n : -X--X--X--X--X-
            pb: -X-
            ps: -X-
            b : -x--X--X--X--X--X-
            s : -x--X--X--X--X--X-
        """
        # Create lists for hidden states (cannot use single tensor as buffer because
        # autograd won't work with inplace writes)
        T = actions.size(0) + 1
        beliefs = [torch.empty(0)] * T
        prior_states = [torch.empty(0)] * T
        prior_means = [torch.empty(0)] * T
        prior_std_devs = [torch.empty(0)] * T
        posterior_states = [torch.empty(0)] * T
        posterior_means = [torch.empty(0)] * T
        posterior_std_devs = [torch.empty(0)] * T
        beliefs[0] = prev_belief
        prior_states[0] = prev_state
        posterior_states[0] = prev_state
        
        for t in range(T - 1):
            s_t = prior_states[t] if observations is None else posterior_states[t]
            a_t = actions[t]
            b_t = beliefs[t]

            # print("STATE", s_t.shape)  # (b, c)
            # print("ACT", a_t.shape)    # (b, c)
            # print("BELIEF", b_t.shape) # (b, c)
            
            # Update belief (deterministic RNN hidden state)
            rnn_in = self.act_fn(self.fc_embed_state_action(torch.cat([s_t, a_t], dim=-1)))
            b_tp1 = self.rnn(rnn_in, b_t)
            beliefs[t + 1] = b_tp1
            # Compute state prior by applying transition dynamics
            prior_out = self.fc_state_prior(self.act_fn(self.fc_embed_belief_prior(b_tp1)))
            prior_means[t + 1], log_prior_std_dev = torch.chunk(prior_out, 2, dim=1)
            prior_std_dev = torch.exp(log_prior_std_dev)
            prior_std_devs[t + 1] = F.softplus(prior_std_dev) + self.min_std_dev
            prior_states[t + 1] = prior_means[t + 1] + prior_std_devs[t + 1] \
                                  * torch.randn_like(prior_means[t + 1])     
            if observations is not None:
                # Compute state posterior by applying transition dynamics on current observation
                embed_in = torch.cat([b_tp1, observations[t]], dim=1)
                posterior_in = self.act_fn(self.fc_embed_belief_posterior(embed_in))
                posterior_out = self.fc_state_posterior(posterior_in)
                posterior_means[t + 1], log_posterior_std_dev = torch.chunk(posterior_out, 2,
                                                                            dim=1)
                posterior_std_dev = torch.exp(log_posterior_std_dev)
                posterior_std_devs[t + 1] = F.softplus(posterior_std_dev) + self.min_std_dev
                posterior_states[t + 1] = posterior_means[t + 1] + posterior_std_devs[t + 1] \
                                          * torch.randn_like(posterior_means[t + 1])
        # Return new hidden states
        hidden = _RSSMReturn(beliefs=torch.stack(beliefs[1:], dim=0),
                             prior_states=torch.stack(prior_states[1:], dim=0),
                             prior_means=torch.stack(prior_means[1:], dim=0),
                             prior_std_devs=torch.stack(prior_std_devs[1:], dim=0))
        if observations is not None:
            hidden.posterior_states = torch.stack(posterior_states[1:], dim=0)
            hidden.posterior_means = torch.stack(posterior_means[1:], dim=0)
            hidden.posterior_std_devs = torch.stack(posterior_std_devs[1:], dim=0)
        return hidden

    
if __name__ == '__main__':
    from torchsummary import summary

    device = 'cpu'

    model = ImageEncoder(3, 256).to(device)
    summary(model, (3, 64, 64), device=device)

    model = ImageDecoder(3, 256).to(device)
    summary(model, (256,), device=device)
