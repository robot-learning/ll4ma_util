"""
Note that functions in this file are dependent on apriltag repos and will
require adding some dependencies.
In particular, pip install dt-apriltags is required for this script.
You can also install apriltag ros to get the tags from a rostopic via the installation
of the following repos:
git clone https://github.com/AprilRobotics/apriltag.git      # Clone Apriltag library
git clone https://github.com/AprilRobotics/apriltag_ros.git  # Clone Apriltag ROS wrapper

pip install dt-apriltags
"""

import os
import scipy
import numpy as np

from dt_apriltags import Detector

import copy
import cv2

import open3d as o3d

import rospy
import tf2_ros
from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud
from geometry_msgs.msg import TransformStamped
from sensor_msgs.msg import CameraInfo

from ll4ma_util import ros_util, math_util
from ll4ma_util import pointcloud_util as pc_util


def camera_opt(init_wTcam, apts, target_pts):
    """
    init_wTcam: (4, 4) camera pose estimation
    apts: (n, 4, 3) apriltag points, the corners for each image in image frame
    target_pts: (n, 4, 3) apriltag points as defined by the transform from world to end effector

    optimize over the camera pose to find a pose that minimizes the error between
    the estimated apriltag corners from the image projected into the real world and
    the actual apriltag corners on the robot hand.
    """

    x0 = init_wTcam.copy()
    iterates = [x0.squeeze().copy()]
    max_iterations = 100

    def residuals(x):
        # apply the x transform to the apts to get the real world projection

        # compare to target_pts
        res = target_pts - x
        return res

    def jacobian(x):
        # jacobian of the x transformation of the apts
        jac = -(x)
        return jac

    def get_iterates(x):
        iterates.append(x)

    # bounds = scipy.optimize.Bounds(
    #     lb=self.problem.min_bounds.numpy(),
    #     ub=self.problem.max_bounds.numpy()
    # )

    result = scipy.optimize.least_squares(
        residuals,
        x0,
        jac=jacobian,
        # bounds=bounds,
        callback=get_iterates,
        max_nfev=max_iterations,
    )

    return result


def get_camera_estimate(detector, rgb, tag_pose, tag_size, camera_params, visualization=False):
    grey_img = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)

    if isinstance(camera_params, np.ndarray):
        assert camera_params.shape == (3, 3)
        cameraMatrix = camera_params
    else:
        assert isinstance(camera_params, CameraInfo)
        cameraMatrix = np.array(camera_params.K).reshape((3, 3))
    camera_params = (cameraMatrix[0, 0], cameraMatrix[1, 1], cameraMatrix[0, 2], cameraMatrix[1, 2])
    tags = detector.detect(grey_img, True, camera_params, tag_size)
    if len(tags) > 0:
        print(tags)
    else:
        print("no tags found!!!")

    if visualization:
        img = rgb.copy()
        for tag in tags:
            for idx in range(len(tag.corners)):
                cv2.line(
                    img,
                    tuple(tag.corners[idx - 1, :].astype(int)),
                    tuple(tag.corners[idx, :].astype(int)),
                    (0, 255, 0),
                )

            cv2.putText(
                img,
                str(tag.tag_id),
                org=(tag.corners[0, 0].astype(int) + 10, tag.corners[0, 1].astype(int) + 10),
                fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=0.8,
                color=(0, 0, 255),
            )

        cv2.imshow("Detected tags", img)

        k = cv2.waitKey(0)
        if k == 27:  # wait for ESC key to exit
            cv2.destroyAllWindows()

    wTtag = tag_pose

    tag = tags[0]
    camTtag = np.eye(4)
    camTtag[:3, :3] = tag.pose_R.copy()
    camTtag[:3, 3] = tag.pose_t.reshape(-1).copy()
    tagTcam = math_util.homogeneous_inverse(camTtag)

    wTcam = wTtag @ tagTcam

    print("wTcamera:", wTcam)

    return wTcam, wTtag


class RobotposeCameraCalibrator:
    """docstring for RobotposeCameraCalibrator."""

    def __init__(
        self,
        tag_family="tag36h11",
        tag_size=0.037,
        rgb_topic=None,
        depth_topic=None,
        info_topic=None,
        pc_topic=None,
        joint_topic=None,
    ):
        from sensor_msgs.msg import JointState, PointCloud2, CameraInfo
        from sensor_msgs.msg import Image as msgImage

        self.tag_size = tag_size  # meters
        self.tag_family = tag_family
        self.at_detector = Detector(
            families=self.tag_family,
            nthreads=1,
            quad_decimate=1.0,
            quad_sigma=0.0,
            refine_edges=1,
            decode_sharpening=0.25,
            debug=0,
        )

        if rgb_topic is None:
            rgb_topic = "/camera/color/image_raw"
        if depth_topic is None:
            depth_topic = "/camera/aligned_depth_to_color/image_raw"
        if info_topic is None:
            info_topic = "/camera/depth/camera_info"
        if pc_topic is None:
            pc_topic = "/camera/depth/color/points"
        if joint_topic is None:
            joint_topic = "/left/iiwa/joint_states"

        rospy.Subscriber(rgb_topic, msgImage, self.rgb_cb)
        rospy.Subscriber(depth_topic, msgImage, self.depth_cb)
        rospy.Subscriber(info_topic, CameraInfo, self.camera_cb)
        rospy.Subscriber(pc_topic, PointCloud2, self.pc_cb)
        rospy.Subscriber(joint_topic, JointState, self.joints_cb)

        self.rgb_msg = None
        self.depth_msgs = []
        self.max_depth_num = 3
        self.info_msg = None
        self.pc_msg = None
        self.joints = None

    def collect_poses(self):
        """
        This function moves the robot to multiple poses and collects the
        rgbd images and apriltag estimates at each.
        """

        # TODO: move the robot to a pose and get the camera estimate

        # combine the estimates into one camera estimate

    def estimate_camera_pose(self, joints=None, rgb=None, camera_params=None, visualization=False):
        if joints is None:
            joints = self.get_joints()
        if rgb is None:
            rgb, depth = self.get_image_info()
        if camera_params is None:
            camera_params = self.camera_params

        grey_img = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)

        tags = self.at_detector.detect(grey_img, True, camera_params, self.tag_size)
        if len(tags) > 0:
            print(tags)
        else:
            print("no tags found!!!")
            return None, None

        if visualization:
            img = rgb.copy()
            for tag in tags:
                for idx in range(len(tag.corners)):
                    cv2.line(
                        img,
                        tuple(tag.corners[idx - 1, :].astype(int)),
                        tuple(tag.corners[idx, :].astype(int)),
                        (0, 255, 0),
                    )

                cv2.putText(
                    img,
                    str(tag.tag_id),
                    org=(tag.corners[0, 0].astype(int) + 10, tag.corners[0, 1].astype(int) + 10),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=0.8,
                    color=(0, 0, 255),
                )

            cv2.imshow("Detected tags", img)

            k = cv2.waitKey(0)
            if k == 27:  # wait for ESC key to exit
                cv2.destroyAllWindows()

        # TODO: set the tag pose to be defined by the end effector pose
        wTtag = np.eye(4)
        # tag pose is centered with robot and forward by 0.3048
        table_height = 0.596
        wTtag[0, 3] = 0.305
        wTtag[1, 3] = 0.0
        wTtag[2, 3] = 0.1397 + table_height
        # if upright (marker facing -x instead of z):
        wTtag[:3, :3] = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]]).T
        assert np.allclose(np.eye(3), wTtag[:3, :3].T @ wTtag[:3, :3])

        tag = tags[0]
        camTtag = np.eye(4)
        camTtag[:3, :3] = tag.pose_R.copy()
        camTtag[:3, 3] = tag.pose_t.reshape(-1).copy()
        tagTcam = math_util.homogeneous_inverse(camTtag)

        wTcam = wTtag @ tagTcam

        print("wTcamera:", wTcam)

        return wTcam, wTtag

    def broadcast_camera_pose(self, wTcamera=None, wTtag=None, display_pc=False):
        if wTcamera is None:
            wTcamera, wTtag = self.estimate_camera_pose(visualization=display_pc)
        # if tf broadcast accounts for camera modifier then undo it before broadcast
        camera_modT = np.eye(4)
        camera_modT[:3, :3] = math_util.get_x_axis_rotation(
            np.array([-np.pi / 2])
        ) @ math_util.get_y_axis_rotation(np.array([np.pi / 2]))
        pose = wTcamera.copy()
        pose[:3, :3] = pose[:3, :3] @ camera_modT.T[:3, :3]
        # camera modifier has no translation so next line doesnt do anything
        # wTcamera[:3, 3] += wTcamera[:3, :3] @ camera_modT.T[:3, 3]

        pose = ros_util.homogeneous_to_pose(pose)
        tf_stmp = TransformStamped()
        tf_stmp.header.stamp = rospy.Time.now()
        tf_stmp.header.frame_id = "world"
        tf_stmp.child_frame_id = "camera_link"
        tf_stmp.transform.translation.x = pose.position.x
        tf_stmp.transform.translation.y = pose.position.y
        tf_stmp.transform.translation.z = pose.position.z
        tf_stmp.transform.rotation.x = pose.orientation.x
        tf_stmp.transform.rotation.y = pose.orientation.y
        tf_stmp.transform.rotation.z = pose.orientation.z
        tf_stmp.transform.rotation.w = pose.orientation.w

        broadcaster = tf2_ros.StaticTransformBroadcaster()

        print("broadcasting transform", tf_stmp)
        broadcaster.sendTransform(tf_stmp)

        for _ in range(3):
            if rospy.is_shutdown():
                return
            rospy.sleep(0.1)
            broadcaster.sendTransform(tf_stmp)

        if display_pc:
            pose = ros_util.homogeneous_to_pose(wTcamera)
            tf_stmp = TransformStamped()
            tf_stmp.header.stamp = rospy.Time.now()
            tf_stmp.header.frame_id = "world"
            tf_stmp.child_frame_id = "camera_link"
            tf_stmp.transform.translation.x = pose.position.x
            tf_stmp.transform.translation.y = pose.position.y
            tf_stmp.transform.translation.z = pose.position.z
            tf_stmp.transform.rotation.x = pose.orientation.x
            tf_stmp.transform.rotation.y = pose.orientation.y
            tf_stmp.transform.rotation.z = pose.orientation.z
            tf_stmp.transform.rotation.w = pose.orientation.w
            pc_msg = do_transform_cloud(self.pc_msg, tf_stmp)
            pc_msg, clr = ros_util.pointcloud2_msg_to_numpy(pc_msg)
            pc_msg = np.concatenate((pc_msg, clr), -1)
            og_p = o3d.geometry.TriangleMesh.create_coordinate_frame()
            og_p.scale(0.15, (0, 0, 0))
            cen_p = copy.deepcopy(og_p)
            cen_p = cen_p.transform(wTcamera)
            tag_p = copy.deepcopy(og_p)
            tag_p = tag_p.transform(wTtag)
            o3d.visualization.draw_geometries(
                [pc_util.to_o3d_pc(pc_msg), cen_p, tag_p, og_p],
            )

    def get_image_info(self):
        if self.rgb_msg is None:
            print("waiting for rgb_msg")
        if len(self.depth_msgs) < self.max_depth_num:
            print("waiting for depth_msgs")
        cont = self.rgb_msg is None or len(self.depth_msgs) < self.max_depth_num
        while not rospy.is_shutdown() and cont:
            rospy.sleep(0.1)
            cont = self.rgb_msg is None or len(self.depth_msgs) < self.max_depth_num

        depths = [ros_util.msg_to_depth(msg) for msg in self.depth_msgs]
        idx = [i for i in range(len(depths)) if (depths[i] > 0).sum() > 10000]
        depths = [depths[i] for i in idx]

        depth = np.stack(depths, -1)
        # depth_max = depth.max()
        # depth[(depth == 0).any(-1)] *= 0
        # depth[(depth == depth_max).any(-1)] = depth_max
        depth = np.median(depth, -1)

        # filter depth
        depth = depth.astype(np.float32)
        depth = cv2.medianBlur(depth, 5)

        rgb = ros_util.msg_to_rgb(self.rgb_msg)
        rgb = np.stack((rgb[..., 2], rgb[..., 1], rgb[..., 0]), -1)

        return rgb, depth

    def get_joints(self):
        if self.joints is None:
            print("waiting for joints")
        while not rospy.is_shutdown() and self.joints is None:
            rospy.sleep(0.1)
        joints = np.array(self.joints.position)
        return joints

    @property
    def camera_params(self):
        cameraMatrix = np.array(self.info_msg.K).reshape((3, 3))
        camera_params = (
            cameraMatrix[0, 0],
            cameraMatrix[1, 1],
            cameraMatrix[0, 2],
            cameraMatrix[1, 2],
        )
        return camera_params

    def rgb_cb(self, msg):
        self.rgb_msg = msg

    def depth_cb(self, msg):
        self.depth_msgs.append(msg)

    def camera_cb(self, msg):
        self.info_msg = msg

    def pc_cb(self, msg):
        self.pc_msg = msg

    def joints_cb(self, msg):
        self.joints = msg


def main():
    import torch

    rospy.init_node("april_calibration")

    print("\nTESTING WITH ROS IMAGE")

    calibrator = RobotposeCameraCalibrator()

    calibrator.broadcast_camera_pose(display_pc=False)
    return

    filename = "~/data/calibration/apriltag_calibration(2024-6-25-17-32)_mini.torch"
    filename = os.path.expanduser(filename)

    data = torch.load(filename)
    # tmp_data = {
    #     'world_corners': self.world_corners,
    #     'img_corners': self.img_corners,
    #     'worldTcameras': self.worldTcameras,
    #     'camera_info': self.depth_camera_info,
    # }
    init_wTcam = data["worldTcameras"]
    apts = data["img_corners"]
    target_pts = data["world_corners"]
    camera_opt(init_wTcam, apts, target_pts)


if __name__ == "__main__":
    main()
