"""
In general the torch.distributions library has most of what you need, but I
need some simple functionality for a GMM (fitting and sampling) in batch
that isn't immediately offered by torch. You can do MixtureSameFamily but
I don't think you can fit with that. I'm implementing my own here for
what I need, not even as a proper torch distribution, so should probably
extend what's here to be a proper torch distribution that you can use
in e.g. torch's KL divergence.
"""
import math

import torch
from torch.distributions import MultivariateNormal, Categorical, VonMises, Normal

import numpy as np
from sklearn.mixture import GaussianMixture as scipy_GMM

from ll4ma_util.torch_util import ortho6d_to_rotation, rotation_Exp_map


class GaussianMixtureModel:

    def __init__(self, alpha=None, mu=None, sigma=None, sigma_tril=None, temperature=1):
        """
        Sizes:
            B - Batch size
            K - Number of components
            D - Size of data vector

        Args:
            alpha (Tensor): Component weights of shape (B, K)
            mu (Tensor): Means of shape (B, D, K)
            sigma (Tensor): Covariance Matrix of shape (B, D, D, K)
            sigma_tril (Tensor): Lower-triangular covariance (cholesky) of shape (B, D, D, K)
        """
        self.DistClass = MultivariateNormal
        self.set_mvns(alpha, mu, sigma, sigma_tril, temperature=temperature)
    @property
    def batch_size(self):
        return self.mu.size(0)

    @property
    def vec_size(self):
        return self.mu.size(1)

    @property
    def n_components(self):
        if self.mu is not None:
            return self.mu.size(2)
        else:
            return -1

    def get_max_component(self):
        max_idx = torch.argmax(self.alpha, dim=-1).unsqueeze(-1)
        mu = self.mu[:,:,0]
        sigma = self.sigma[:,:,:,0]
        for i in range(1, self.n_components):
            mu = mu.where(max_idx != i, self.mu[:,:,i])
            sigma = sigma.where(max_idx.unsqueeze(-1) != i, self.sigma[:,:,:,i])
        mvn = self.DistClass(mu, sigma)
        return mvn

    def get_component(self, idx):
        mu = self.mu[:,:,idx]
        sigma = self.sigma[:,:,:,idx]
        mvn = self.DistClass(mu, sigma)
        return mvn
        
    def get_mean(self, mix_idx=None):
        # TODO just returning most likely component mean
        D = self.mu.shape[1]
        if mix_idx is None:
            mix_idx = torch.argmax(self.alpha, dim=-1).unsqueeze(-1).repeat(1, 1, D)
        mu = self.mu[:,:,0]
        for i in range(1, self.n_components):
            mu = mu.where(mix_idx != i, self.mu[:,:,i])
        return mu, mix_idx[:,:,0]
        
    def set_mvns(self, alpha, mu, sigma=None, sigma_tril=None, temperature=1):
        self.alpha = alpha
        self.mu = mu
        if sigma is not None:
            self.sigma = sigma.clone()
            B = self.batch_size
            D = sigma.shape[-2]#self.vec_size
            K = self.n_components
            sigma = sigma.permute(0, 3, 1, 2).reshape(B * K, D, D)
            self.sigma_tril = torch.linalg.cholesky(
                sigma
            ).reshape(B, K, D, D).permute(0, 2, 3, 1)
        elif sigma_tril is not None:
            self.sigma_tril = sigma_tril.clone()
            B = self.batch_size
            D = sigma_tril.shape[-2]#self.vec_size
            K = self.n_components
            sigma_tril = sigma_tril.permute(0, 3, 1, 2).reshape(B * K, D, D)
            sigma = torch.bmm(sigma_tril, sigma_tril.transpose(1, 2))
            self.sigma = sigma.reshape(B, K, D, D).permute(0, 2, 3, 1)

        self.mvns = []
        self.mix = None
        if self.mu is not None:
            for comp_idx in range(self.n_components):
                self.mvns.append(
                    self.DistClass(
                        self.mu[:,:,comp_idx],
                        scale_tril=self.sigma_tril[:,:,:,comp_idx] * temperature
                    )
                )
            self.mix = Categorical(self.alpha) # This would always return 0s for (N, 1) input
    
    def sample(self, n_samples=1, mix_idxs = None):
        N = n_samples
        B = self.batch_size
        D = self.vec_size
        K = self.n_components

        # Generate samples from every component MVN
        gmm_samples = torch.zeros(N, B, D, K)
        for comp_idx in range(K):
            gmm_samples[:,:,:,comp_idx] = self.mvns[comp_idx].sample((N,))

        if torch.cuda.is_available():
            gmm_samples = gmm_samples.to('cuda:0')
            
        # Sample which components will be selected to generate MVN samples from
        if mix_idxs is None:
            mix_idxs = self.mix.sample((N,)).unsqueeze(-1).repeat(1, 1, D)
            # Hack for test all select only mode 0:
            mix_idxs = mix_idxs.to(gmm_samples.device)
        else:
            if mix_idxs.shape == (N,B):
                mix_idxs = mix_idxs.unsqueeze(-1).repeat(1, 1, D)
            assert mix_idxs.shape == (N,B,D), "Something wrong with mix_idx processing."

        # Aggregate the selected MVN samples from the different components
        samples = torch.zeros(N, B, D).to(gmm_samples.device)
        for comp_idx in range(K):
            # Take samples from this component that were selected and leave other samples as-is
            samples = samples.where(mix_idxs != comp_idx, gmm_samples[:,:,:,comp_idx])

        return samples, mix_idxs[:,:,0]#.squeeze()  # Don't want to return repeats

    def fit(self, samples, n_components=1, init_params='k-means++'):
        """
        Fits a (batched) GMM given data samples of size (N, B, D).

        Note I don't think there's an implementation in torch to do this already,
        and I'm not implementing EM myself, so hacking for now and just iterating
        over the batch to use scipy's GMM fit.
        """
        B = samples.size(1)
        D = samples.size(-1)
        K = self.n_components
        alphas = []
        mus = []
        sigmas = []
        for batch_idx in range(B):
            gmm = scipy_GMM(n_components=n_components)
            gmm.init_params = init_params
            gmm.fit(samples[:,batch_idx,:].numpy())
            alphas.append(gmm.weights_)
            mus.append(gmm.means_)
            sigmas.append(gmm.covariances_)
        alpha = torch.tensor(np.array(alphas))
        mu = torch.tensor(np.array(mus)).permute(0, 2, 1)
        sigma = torch.tensor(np.array(sigmas)).permute(0, 2, 3, 1)
        self.set_mvns(alpha, mu, sigma)

    def sigma_points(self, beta=2, include_mean=True):
        D = self.vec_size
        n_sigma = 2 * D + 1 if include_mean else 2 * D
        sps = self.mu.unsqueeze(1).repeat(1, n_sigma, 1, 1)
        for comp_idx in range(self.n_components):
            for sp_idx in range(D):
                sps[:,sp_idx,:,comp_idx] += beta * self.sigma_tril[:,sp_idx,:,comp_idx]
                sps[:,sp_idx+D,:,comp_idx] -= beta * self.sigma_tril[:,sp_idx,:,comp_idx]
        return sps  # (B, n_sigma, D, K)

    def log_pdf(self, x, mix_idx=None):
        # Assuming x is shape (n_points, B, D)
        n_points = x.size(0)
        B = x.size(1)#self.batch_size
        D = self.vec_size
        K = self.n_components
        vals = torch.zeros(n_points, B, K)
        for comp_idx in range(K):
            vals[:,:,comp_idx] = self.mvns[comp_idx].log_prob(x) + self.alpha[:,comp_idx].log()
        if mix_idx is None:
            # Gradient vanished with the naive version (see below)
            # return vals.exp().clamp(min=1e-12).sum(dim=-1).log().clamp(min=-1e15, max=1e15)
            
            # applying log over sum using soft relu trick
            # log(a + b) = log_a + log(1 + exp(log_b - log_a))
            # this is more stable during backward, as exp and log explodes
            # for individual small probabilities (here everything is accumulated)
            # NOTE: In theory this should work even if value of a is small
            # but choosing x_max for 'a' is more stable
            max_comp = vals.max(dim=-1)[0]
            exp_logx_div_x_max = (vals - max_comp.unsqueeze(-1)).exp()
            return max_comp + exp_logx_div_x_max.sum(dim=-1).log()
        else:
            if mix_idx.shape == (n_points, B, D):
                mix_idx = mix_idx[:,:,0]
            assert mix_idx.shape == (n_points, B), "Something wrong with mix_idx processing"
            x_ids, y_ids = torch.meshgrid(torch.arange(n_points),
                                          torch.arange(B),
                                          indexing='ij')
            return vals[x_ids, y_ids, mix_idx]

    def likely_comp(self, x):
        n_points = x.size(0)
        B = x.size(1)#self.batch_size
        D = self.vec_size
        K = self.n_components
        vals = torch.zeros(n_points, K)
        for comp_idx in range(K):
            vals[:,comp_idx] = self.mvns[comp_idx].log_prob(x) + self.alpha[:,comp_idx].log()
        return vals.max(dim=-1)[1]

    def get_eps(self, x, mix_idx=None):
        N = x.size(0)
        B = x.size(1)#self.batch_size
        D = self.vec_size
        K = self.n_components
        
        eps_all = torch.zeros(N, B, K)
        for comp_idx in range(K):
            eps_all[:,:,comp_idx] = torch.bmm((x-self.mvns[comp_idx].loc).unsqueeze(1),
                                                self.mvns[comp_idx].precision_matrix).squeeze(1)

        comp_ids = self.likely_comp(x)

        x_ids, y_ids = torch.meshgrid(torch.arange(N),
                                          torch.arange(B),
                                          indexing='ij')
        
        eps = eps_all[x_ids, y_ids, comp_ids.view(-1,1).tile(1,self.vec_size)]
        return eps

    def rcompute(self, eps, comp_ids):
        K = self.n_components
        sample = torch.zeros(eps.shape + torch.Size([K]))
        for comp_idx in range(K):
            sample[:, :, comp_idx] = self.mvns[comp_idx].loc + \
                torch.bmm(eps.detach().unsqueeze(1),
                          self.mvns[comp_idx].covariance_matrix).squeeze(1)
        x_ids, y_ids = torch.meshgrid(torch.arange(eps.shape[0]),
                                      torch.arange(eps.shape[1]),
                                      indexing='ij')
        return sample[x_ids, y_ids, comp_ids.view(-1,1).tile(1,self.vec_size)]


class RotMixtureModel(GaussianMixtureModel):
    def __init__(self, alpha, distributions):
        batch_size, n_components = alpha.size()
        self.alpha = alpha
        assert len(distributions) == n_components, "num alphas and num distributions are not same"
        self.mvns = distributions
        self.mix = Categorical(self.alpha)
        self.__batch_size, self.__vec_size = self.mvns[0].sample(1,).squeeze(0).size()

    @property
    def batch_size(self):
        return self.__batch_size

    @property
    def vec_size(self):
        return self.__vec_size

    @property
    def n_components(self):
        return self.alpha.size(-1)
    
class SE3MixtureModel(GaussianMixtureModel):

    def __init__(self, alpha=None, mu=None, sigma=None, sigma_tril=None, temperature=1):
        """
        Sizes:
            B - Batch size
            K - Number of components
            D - Size of data vector

        Args:
            alpha (Tensor): Component weights of shape (B, K)
            mu (Tensor): Means of shape (B, D, K)
            sigma (Tensor): Covariance Matrix of shape (B, D, D, K)
            sigma_tril (Tensor): Lower-triangular covariance (cholesky) of shape (B, D, D, K)
        """
        self.DistClass = SE3MVN
        self.set_mvns(alpha, mu, sigma, sigma_tril, temperature=temperature)

    def fit(self, samples, n_components=1, init_params='k-means++'):
        raise NotImplementedError

    @property
    def vec_size(self):
        return 12

            
class KDE:
    def __init__(self, data):
        self.N = 0
        self.means = None
        self.covs = None
        self.dist = None
        self.fit(data)
        #raise NotImplementedError('need to store params and checkpoint')

    def fit(self, data):
        '''
        data - (N, D) array
        '''
        assert len(data.shape) == 2, 'data needs to be 2 dimensional - (N,D)'
        N, D = data.shape
        self.means = data

        medians = torch.ones(N, D, 1)
        
        # idx = torch.arange(N)
        # i_s, j_s= torch.meshgrid(idx, idx, indexing='ij')
        # vals = (data[i_s] - data) ** 2
        for i, pt in enumerate(data):
            vals = (data - pt) ** 2
            medians[i, :, 0] = vals.median(dim=0)[0]
        self.covs = torch.eye(D).to(medians.dtype).repeat(N, 1, 1) \
            * medians
        self.means = self.means.to(medians.dtype)
        self.dist = MultivariateNormal(self.means, self.covs)

    def log_prob(self, x):
        assert len(x.shape) == 2, 'data needs to be 2 dimensional - (N,D)'
        assert self.dist is not None, 'No distribution, fit model first'
        vals = self.dist.log_prob(x.unsqueeze(1))
        # Below repeats the same as GMM log pdfs as above, may be make it is util func
        max_comp = vals.max(dim=-1)[0]
        exp_logx_div_x_max = (vals - max_comp.unsqueeze(-1)).exp()
        return max_comp + exp_logx_div_x_max.sum(dim=-1).log()


class RotMVN:
    '''
    MVN for rotations, that implements Gaussian Distributions on Riemannian manifolds
    '''
    def __init__(self, loc,
                 covariance_matrix = None,
                 precision_matrix = None,
                 scale_tril = None):
        self.loc = loc
        if self.loc.dim() < 3:
            self.loc = self.loc.unsqueeze(0)
        self._loc_R_ref = self.loc.mT
        
        if (covariance_matrix is not None) + (scale_tril is not None) + (
            precision_matrix is not None
        ) != 1:
            raise ValueError(
                "Exactly one of covariance_matrix or precision_matrix or scale_tril may be specified."
            )

        if precision_matrix is not None:
            raise NotImplementedError(
                "Precision matrix handling is not implemented yet ")

        if scale_tril is not None:
            self.scale_tril = scale_tril
            #TODO: Compute and store covariance_matrix for consistency

        if covariance_matrix is not None:
            self.covariance_matrix = covariance_matrix
            self.scale_tril = torch.linalg.cholesky(covariance_matrix)

        self.batch_shape = torch.broadcast_shapes(self.loc.shape[:-2],
                                                  self.scale_tril.shape[:-2])

    def _matrix_Log(R):
        '''
        TODO: Will be moving this to torch_utils
        '''
        _eps = 1e-5
        tidx = torch.arange(3)
        trace = R[..., tidx, tidx].sum(-1)
        hi_idx = (trace.abs() - 1) > 2.0
        R[hi_idx] = torch.nn.functional.normalize(R[hi_idx], p=2.0, dim=1)
        # make the value just tiny bit smaller to avoid acos limits
        cos_val = 0.5 * (trace - 1)
        # clamping because arccos is not stable near -1 and 1.
        cos_val = cos_val.clamp(min = -(1 - 1e-5),
                                max =  (1 - 1e-5))
        theta = torch.arccos(cos_val)
        up_ids = torch.where(theta.abs() > 1e-10)[0]
        axis = torch.zeros(R.shape[:-1]).to(R.device, R.dtype)
        rot_skew = (R - R.mT) / (2 * torch.sin(theta))[..., None, None]
        axis[up_ids, 0] = rot_skew[up_ids, 2, 1]
        axis[up_ids, 1] = rot_skew[up_ids, 0, 2]
        axis[up_ids, 2] = rot_skew[up_ids, 1, 0]
        return axis * theta[..., None]
        
    def _Log_loc(self, x):
        '''
        Assuming loc and x are in shape (N, 6)
        '''
        if self._loc_R_ref.shape[0] == x.shape[0]:
            _loc_R_target = torch.bmm(self._loc_R_ref, x)
        else:
            _loc_R_target = self._loc_R_ref @ x
        return type(self)._matrix_Log(_loc_R_target)

    def log_prob(self, x):
        diff = self._Log_loc(x)
        M = type(self)._mahalanobis(self.scale_tril, diff).squeeze(-1)
        half_log_det = self.scale_tril.diagonal(dim1=-2, dim2=-1).sum(-1)
        return -0.5 * (3 * math.log(2 * math.pi) + M) - half_log_det

    def _mahalanobis(L, diff):
        return torch.linalg.solve_triangular(L, diff.unsqueeze(-1), upper=False).pow(2).sum(-2)


    def sample(self, shape):
        if not isinstance(shape, torch.Size):
            shape = torch.Size(shape)
        extended_shape = torch.Size(shape + self.batch_shape +
                                    torch.Size(self.scale_tril.shape[-1:]))
        eps = torch.normal(0, 1, extended_shape).unsqueeze(-1).to(self.loc.device,
                                                                  self.loc.dtype)
        tangent_samples = torch.matmul(self.scale_tril, eps).squeeze(-1)
        return self.loc @ rotation_Exp_map(tangent_samples)


class ExtendedRotMVN:
    def __init__(self, loc,
                 covariance_matrix = None,
                 precision_matrix = None,
                 scale_tril = None,
                 num_rotations = 1):
        self.loc = loc
        self.num_rots = num_rotations
        e_size = self.loc.size(-1) - 6 * self.num_rots
        self.euclidean_loc = loc[:, :e_size]
        self.rotation_locs = [ortho6d_to_rotation(loc[:, e_size+i*6:e_size+(i+1)*6]) \
                              for i in range(self.num_rots)]

        if (covariance_matrix is not None) + (scale_tril is not None) + (
            precision_matrix is not None
        ) != 1:
            raise ValueError(
                "Exactly one of covariance_matrix or precision_matrix or scale_tril may be specified."
            )

        if precision_matrix is not None:
            raise NotImplementedError(
                "Precision matrix handling is not implemented yet ")

        if scale_tril is not None:
            self.euclidean_scale_tril = scale_tril[:, :e_size, :e_size]
            self.rotation_scale_trils = [scale_tril[:, e_size+i*3:e_size+(i+1)*3,
                                            e_size+i*3:e_size+(i+1)*3] for i in range(self.num_rots)]
            #TODO: Compute and store covariance_matrix for consistency

        if covariance_matrix is not None:
            self.euclidean_covariance_matrix = covariance_matrix[:, :e_size, :e_size]
            self.euclidean_scale_tril = torch.linalg.cholesky(self.euclidean_covariance_matrix)

            self.rotation_covariance_matrices = [covariance_matrix[:, e_size+i*3:e_size+(i+1)*3,
                                            e_size+i*3:e_size+(i+1)*3] for i in range(self.num_rots)]
            self.rotation_scale_trils = [torch.linalg.cholesky(rot_cov) for rot_cov in self.rotation_covariance_matrices]

        self.euclidean_mvn = MultivariateNormal(self.euclidean_loc,
                                              scale_tril=self.euclidean_scale_tril)
        self.rotation_mvns = []
        for i in range(self.num_rots):
            self.rotation_mvns.append(RotMVN(self.rotation_locs[i],
                                             scale_tril=self.rotation_scale_trils[i]))

    def sample(self, shape):
        if isinstance(shape, int):
            shape = torch.Size((shape,))
        euclidean_samples = self.euclidean_mvn.sample(shape)
        rotation_shape = torch.Size(shape + self.rotation_mvns[0].batch_shape + (9,))
        samples = [euclidean_samples]
        for rot_mvn in self.rotation_mvns:
            samples.append(rot_mvn.sample(shape).reshape(rotation_shape))
        return torch.cat(samples, dim=-1)

    def log_prob(self, x):
        if x.dim() == 2:
            x = x.unsqueeze(0)
        N, B, D = x.size()
        e_size = self.euclidean_loc.size(-1)
        x_euclidean = x[..., :e_size]

        if D - e_size == self.num_rots * 6:
            x_rotations = [x[..., e_size+i*6:e_size+(i+1)*6] for i in range(self.num_rots)]
            x_rotation_mats = [ortho6d_to_rotation(x_rot.view(-1, 6)) for x_rot in x_rotations]
        elif D - e_size == self.num_rots * 9:
            x_rotation_mats = [x[..., e_size+i*9:e_size+(i+1)*9] for i in range(self.num_rots)]
        
        euclid_prob = self.euclidean_mvn.log_prob(x_euclidean)
        rot_prob = torch.zeros_like(euclid_prob)
        for i in range(self.num_rots):
            rot_prob += self.rotation_mvns[i].log_prob(x_rotation_mats[i].reshape(N, B, 3, 3))
        return euclid_prob + rot_prob

class SE3MVN:
    '''
    MVN that bundles MultivaritaeNormal and RotMVN as SE3 distribution,
    with assuption of no covariance between position orientation

    TODO: To be extended to be joint distribution of position and rotation
    '''
    def __init__(self, loc,
                 covariance_matrix = None,
                 precision_matrix = None,
                 scale_tril = None):
        self.loc = loc
        self.position_loc = loc[:, :3]
        self.rotation_loc = ortho6d_to_rotation(loc[:, 3:])

        if (covariance_matrix is not None) + (scale_tril is not None) + (
            precision_matrix is not None
        ) != 1:
            raise ValueError(
                "Exactly one of covariance_matrix or precision_matrix or scale_tril may be specified."
            )

        if precision_matrix is not None:
            raise NotImplementedError(
                "Precision matrix handling is not implemented yet ")

        if scale_tril is not None:
            self.position_scale_tril = scale_tril[:, :3, :3]
            self.rotation_scale_tril = scale_tril[:, 3:, 3:]
            #TODO: Compute and store covariance_matrix for consistency

        if covariance_matrix is not None:
            self.position_covariance_matrix = covariance_matrix[:, :3, :3]
            self.position_scale_tril = torch.linalg.cholesky(self.position_covariance_matrix)

            self.rotation_covariance_matrix = covariance_matrix[:, 3:, 3:]
            self.rotation_scale_tril = torch.linalg.cholesky(self.rotation_covariance_matrix)

        self.position_mvn = MultivariateNormal(self.position_loc,
                                              scale_tril=self.position_scale_tril)
        self.rotation_mvn = RotMVN(self.rotation_loc,
                                   scale_tril=self.rotation_scale_tril)

    def sample(self, shape):
        if isinstance(shape, int):
            shape = torch.Size((shape,))
        position_samples = self.position_mvn.sample(shape)
        rotation_samples = self.rotation_mvn.sample(shape)
        rotation_shape = torch.Size(shape + self.rotation_mvn.batch_shape + (9,))
        rotation_samples = rotation_samples.reshape(rotation_shape)
        return torch.cat([position_samples, rotation_samples], dim=-1)

    def log_prob(self, x):
        x_position = x[..., :3]
        x_rotation = x[..., 3:]
        if x_rotation.shape[-1] == 6:
            x_rotation = ortho6d_to_rotation(x_rotation.view(-1, 6))

        pos_prob = self.position_mvn.log_prob(x_position)
        rot_prob = self.rotation_mvn.log_prob(x_rotation)
        return pos_prob + rot_prob

    def get_mean(self):
        return torch.cat([self.position_loc, self.rotation_loc.view(-1,9)], dim=-1)
        
    
class CircularSE2:
    '''
    Creates a planar circular or wrapped distribution in polar co-ordinates
    using Von-Mises distribution for radius r.
    '''
    
    def __init__(self, radius_distribution, mean_radian=0.0, concentration=1e-15):
        assert isinstance(radius_distribution, torch.distributions.Distribution), \
            "arg: radius_distribution must be a torch.distributions.Distribution"
        self.r_dist = radius_distribution
        self.o_dist = VonMises(mean_radian, concentration)

    def sample(self, N):
        oN = self.o_dist.loc.numel()
        assert N//oN, f"Num samples requested {N} is not a multiple of number of componets {N}"
        rads = self.o_dist.sample((N//oN,))
        radii = self.r_dist.sample((N,)) # Assuming r_dist is 1 comp
        return radii, rads

    def log_prob(self, dists, rads):
        _rad_query = rads.reshape(-1, self.o_dist.loc.numel())
        rad_prob = self.o_dist.log_prob(_rad_query)
        dist_prob = self.r_dist.log_prob(dists.view(-1, 1))
        #print(self.r_dist.loc)
        #print(torch.vstack([dists.view(-1), dist_prob.exp().view(-1)]).T)
        return rad_prob.view(-1) + dist_prob.view(-1)
        



if __name__ == '__main__':
    # alpha = torch.tensor([0.2, 0.8]).unsqueeze(0).repeat(10, 1)
    # mu = torch.zeros(10, 4, 2)
    # sigma = torch.eye(4).unsqueeze(0).unsqueeze(-1).repeat(10, 1, 1, 2)
    # gmm = GaussianMixtureModel(alpha, mu, sigma)

    gmm = GaussianMixtureModel()
    gmm.fit(torch.randn(100, 10, 4), n_components=2)
    
    print("ALPHA", gmm.alpha.shape)
    print("MU", gmm.mu.shape)
    print("SIGMA", gmm.sigma.shape)
    print("SIGMA TRIL", gmm.sigma_tril.shape)
    print("SAMPLES", gmm.sample(13).shape)
    print("SPS", gmm.sigma_points().shape)
    print("LOG PDF", gmm.log_pdf(gmm.sample(13)).shape)
