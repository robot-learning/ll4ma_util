from ll4ma_util import pytorch3d

from typing import Optional

import torch


def quaternion_l2f(quat: torch.Tensor) -> torch.Tensor:
    """
    Code: Iain Lee
    moves the real part from last to first

    Args:
        quaternions with real part last, as tensor of shape (..., 4).

    Returns:
        quaternions with real part first, as tensor of shape (..., 4).
    """
    batch_dim = quat.shape[:-1]
    if len(quat.shape) == 1:
        quat = quat.unsqueeze(0)
    quat = torch.cat((quat[..., 3:], quat[..., :3]), dim=-1) * -1
    return quat.reshape(batch_dim + (4,))


def quaternion_f2l(quat: torch.Tensor) -> torch.Tensor:
    """
    Code: Iain Lee
    moves the real part from first to last

    Args:
        quaternions with real part first, as tensor of shape (..., 4).

    Returns:
        quaternions with real part last, as tensor of shape (..., 4).
    """
    batch_dim = quat.shape[:-1]
    if len(quat.shape) == 1:
        quat = quat.unsqueeze(0)
    quat = torch.cat((quat[..., 1:], quat[..., :1]), dim=-1) * -1
    return quat.reshape(batch_dim + (4,))


def quaternion_to_matrix(quaternions: torch.Tensor) -> torch.Tensor:
    return pytorch3d.quaternion_to_matrix(quaternion_l2f(quaternions))


def matrix_to_quaternion(matrix: torch.Tensor) -> torch.Tensor:
    """
    Convert rotations given as rotation matrices to quaternions.

    Args:
        matrix: Rotation matrices as tensor of shape (..., 3, 3).

    Returns:
        quaternions with real part first, as tensor of shape (..., 4).
    """
    return quaternion_f2l(pytorch3d.matrix_to_quaternion(matrix))


def euler_angles_to_matrix(euler_angles: torch.Tensor, convention: str) -> torch.Tensor:
    return pytorch3d.euler_angles_to_matrix(euler_angles, convention)


def matrix_to_euler_angles(matrix: torch.Tensor, convention: str) -> torch.Tensor:
    return pytorch3d.matrix_to_euler_angles(matrix, convention)


def random_quaternions(n: int, dtype: Optional[torch.dtype] = None, device=None) -> torch.Tensor:
    return quaternion_f2l(pytorch3d.random_quaternions(n, dtype, device))


def random_rotations(n: int, dtype: Optional[torch.dtype] = None, device=None) -> torch.Tensor:
    return pytorch3d.random_rotations(n, dtype, device)


def random_rotation(dtype: Optional[torch.dtype] = None, device=None) -> torch.Tensor:
    return pytorch3d.random_rotation(dtype, device)


def standardize_quaternion(quaternions: torch.Tensor) -> torch.Tensor:
    return quaternion_f2l(pytorch3d.standardize_quaternion(quaternion_l2f(quaternions)))


def quaternion_raw_multiply(a: torch.Tensor, b: torch.Tensor) -> torch.Tensor:
    return quaternion_f2l(pytorch3d.quaternion_raw_multiply(quaternion_l2f(a), quaternion_l2f(b)))


def quaternion_multiply(a: torch.Tensor, b: torch.Tensor) -> torch.Tensor:
    return quaternion_f2l(pytorch3d.quaternion_multiply(quaternion_l2f(a), quaternion_l2f(b)))


def quaternion_invert(quaternion: torch.Tensor) -> torch.Tensor:
    return quaternion_f2l(pytorch3d.quaternion_invert(quaternion_l2f(quaternion)))


def quaternion_apply(quaternion: torch.Tensor, point: torch.Tensor) -> torch.Tensor:
    return pytorch3d.quaternion_apply(quaternion_l2f(quaternion), point)


def axis_angle_to_matrix(axis_angle: torch.Tensor) -> torch.Tensor:
    return pytorch3d.quaternion_to_matrix(axis_angle_to_quaternion(axis_angle))


def matrix_to_axis_angle(matrix: torch.Tensor) -> torch.Tensor:
    return pytorch3d.matrix_to_axis_angle(matrix)


@torch.jit.script
def axis_angle_to_quaternion(axis_angle: torch.Tensor) -> torch.Tensor:
    return quaternion_f2l(pytorch3d.axis_angle_to_quaternion(axis_angle))


@torch.jit.script
def quaternion_to_axis_angle(quaternions: torch.Tensor) -> torch.Tensor:
    return pytorch3d.quaternion_to_axis_angle(quaternion_l2f(quaternions))


def rotation_6d_to_matrix(d6: torch.Tensor) -> torch.Tensor:
    return pytorch3d.rotation_6d_to_matrix(d6)


def matrix_to_rotation_6d(matrix: torch.Tensor) -> torch.Tensor:
    return pytorch3d.matrix_to_rotation_6d(matrix)
