import os
import copy
import numpy as np
import torch
from matplotlib import colors
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
from time import time

try:
    import cv2
    from cv_bridge import CvBridge
except ModuleNotFoundError:
    pass

import rospy
import rospkg
from ros_numpy import point_cloud2 as pc2

import std_srvs.srv as std_srvs

from sensor_msgs.msg import JointState, PointCloud, ChannelFloat32, Image
from geometry_msgs.msg import TransformStamped, Pose, PoseStamped, Point32, PoseArray

from visualization_msgs.msg import (
    InteractiveMarker,
    InteractiveMarkerControl,
    Marker,
)
from tf2_msgs.msg import TFMessage
from moveit_msgs.msg import (
    DisplayRobotState,
    DisplayTrajectory,
    ObjectColor,
    RobotState,
    RobotTrajectory,
)
from std_msgs.msg import ColorRGBA, Header
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

from ll4ma_util import data_util, math_util, file_util, torch_util

rospack = rospkg.RosPack()


def rgb_to_msg(array):
    return img_to_msg(cv2.cvtColor(array, cv2.COLOR_RGB2BGR))


def msg_to_rgb(msg):
    return cv2.cvtColor(msg_to_img(msg), cv2.COLOR_BGR2RGB)


def seg_to_msg(seg, seg_ids, colors):
    return rgb_to_msg(data_util.segmentation_to_rgb(seg, seg_ids, colors))


def depth_to_msg(array):
    return img_to_msg(array)


def msg_to_depth(msg):
    return msg_to_img(msg)


def img_to_msg(array):
    return CvBridge().cv2_to_imgmsg(array)


def msg_to_img(msg):
    img = CvBridge().imgmsg_to_cv2(msg)
    return img


def flow_to_msg(flow):
    magnitude, angle = cv2.cartToPolar(flow[:, :, 0], flow[:, :, 1])
    flow_img = np.zeros((flow.shape[0], flow.shape[1], 3))
    flow_img[:, :, 0] = angle * 180 / np.pi / 2
    flow_img[:, :, 2] = cv2.normalize(magnitude, None, 0, 255, cv2.NORM_MINMAX)
    flow_img = flow_img.astype(np.uint8)
    flow_img = cv2.cvtColor(flow_img, cv2.COLOR_HSV2BGR)
    flow_msg = CvBridge().cv2_to_imgmsg(flow_img)
    return flow_msg


def msg_to_rgba(msg):
    return msg.r, msg.g, msg.b, msg.a


def rgba_to_msg(r, g, b, a):
    return ColorRGBA(r, g, b, a)


def array_to_pose(arr):
    pose = Pose()
    pose.position.x = arr[0]
    pose.position.y = arr[1]
    pose.position.z = arr[2]
    pose.orientation.x = arr[3]
    pose.orientation.y = arr[4]
    pose.orientation.z = arr[5]
    pose.orientation.w = arr[6]
    return pose


def pose_to_array(pose):
    arr = np.array(
        [
            pose.position.x,
            pose.position.y,
            pose.position.z,
            pose.orientation.x,
            pose.orientation.y,
            pose.orientation.z,
            pose.orientation.w,
        ]
    )
    return arr


def pose_to_homogeneous(pose):
    p = pose_to_position(pose)
    q = pose_to_quaternion(pose)
    T = math_util.pose_to_homogeneous(p, q)
    return T


def pose_to_position(pose):
    return np.array([pose.position.x, pose.position.y, pose.position.z])


def pose_to_quaternion(pose):
    return np.array(
        [pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w]
    )


def pose_to_rotation(pose):
    return math_util.quat_to_rotation(pose_to_quaternion(pose))


def array_to_homogeneous(arr):
    p = arr[:3]
    q = arr[3:]
    T = math_util.pose_to_homogeneous(p, q)
    return T


def homogeneous_to_pose(T):
    try:
        p, q = math_util.homogeneous_to_pose(T)
    except Exception:
        p = torch_util.transform2quatpose(torch.tensor(T)).numpy()
        p, q = p[:3], p[3:7]
    pose = Pose()
    pose.position.x = p[0]
    pose.position.y = p[1]
    pose.position.z = p[2]
    pose.orientation.x = q[0]
    pose.orientation.y = q[1]
    pose.orientation.z = q[2]
    pose.orientation.w = q[3]
    return pose


def homogeneous_to_array(T):
    try:
        p, q = math_util.homogeneous_to_pose(T)
    except Exception:
        return torch_util.transform2quatpose(torch.tensor(T)).numpy()
    arr = np.concatenate((p, q))
    return arr


def get_joint_state_msg(joint_pos=[], joint_vel=[], joint_tau=[], joint_names=[]):
    joint_state_msg = JointState()
    if len(joint_names) > 0:
        joint_state_msg.name = joint_names
    if len(joint_pos) > 0:
        joint_state_msg.position = joint_pos
    if len(joint_vel) > 0:
        joint_state_msg.velocity = joint_vel
    if len(joint_tau) > 0:
        joint_state_msg.effort = joint_tau
    return joint_state_msg


def get_color(color_id, alpha=1):
    """
    Color can be any name from this page:
        https://matplotlib.org/stable/gallery/color/named_colors.html
    """
    if isinstance(color_id, str):
        converter = colors.ColorConverter()
        c = converter.to_rgba(colors.cnames[color_id])
    elif len(color_id) == 3:
        c = list(color_id) + [1]
    else:
        c = color_id
    color = ColorRGBA(*c)
    color.a = alpha
    return color


def get_display_robot_msg(joints, joint_names, link_names=[], color=None, alpha=1.0):
    msg = DisplayRobotState()
    msg.state.joint_state.name = joint_names
    msg.state.joint_state.position = joints
    if color is not None and link_names:
        if isinstance(color, list):
            msg.highlight_links = [
                ObjectColor(id=link, color=get_color(clr)) for link, clr in zip(link_names, color)
            ]
        else:
            msg.highlight_links = [
                ObjectColor(id=link, color=get_color(color)) for link in link_names
            ]
        if isinstance(alpha, list):
            for link, a in zip(msg.highlight_links, alpha):
                link.color.a = a
        else:
            for link in msg.highlight_links:
                link.color.a = alpha
    return msg


def get_tf_msg(tf, idx):
    """
    Constructs a TFMessage for visualizing TF data in rviz.

    TODO (adam): I think I used this for visualizing Isaac Gym data, the input
    data seems specialized for that purpose so should probably make this more
    generic.
    """
    tf_msg = TFMessage()
    for child_frame, data in tf.items():
        tf_msg.transforms.append(
            get_tf_stamped_msg(
                data["position"][idx],
                data["orientation"][idx],
                data["parent_frame"],
                child_frame,
            )
        )
    return tf_msg


def pose_to_tf_stamped_msg(pose, frame_id, child_frame_id):
    """
    Constructs a TransformStamped message from input data.
    """
    if not isinstance(pose, Pose):
        pose = homogeneous_to_pose(pose)
    tf_stmp = TransformStamped()
    tf_stmp.header.frame_id = frame_id
    tf_stmp.child_frame_id = child_frame_id
    tf_stmp.transform.translation.x = pose.position.x
    tf_stmp.transform.translation.y = pose.position.y
    tf_stmp.transform.translation.z = pose.position.z
    tf_stmp.transform.rotation.x = pose.orientation.x
    tf_stmp.transform.rotation.y = pose.orientation.y
    tf_stmp.transform.rotation.z = pose.orientation.z
    tf_stmp.transform.rotation.w = pose.orientation.w
    return tf_stmp


def get_tf_stamped_msg(position, orientation, frame_id, child_frame_id):
    """
    Constructs a TransformStamped message from input data.
    """
    tf_stmp = TransformStamped()
    tf_stmp.header.frame_id = frame_id
    tf_stmp.child_frame_id = child_frame_id
    tf_stmp.transform.translation.x = position[0]
    tf_stmp.transform.translation.y = position[1]
    tf_stmp.transform.translation.z = position[2]
    tf_stmp.transform.rotation.x = orientation[0]
    tf_stmp.transform.rotation.y = orientation[1]
    tf_stmp.transform.rotation.z = orientation[2]
    tf_stmp.transform.rotation.w = orientation[3]
    return tf_stmp


def get_pose_stamped_msg(position=None, orientation=None, frame_id="world"):
    msg = PoseStamped()
    msg.header.frame_id = frame_id
    if position is not None:
        msg.pose.position.x = position[0]
        msg.pose.position.y = position[1]
        msg.pose.position.z = position[2]
    if orientation is not None:
        msg.pose.orientation.x = orientation[0]
        msg.pose.orientation.y = orientation[1]
        msg.pose.orientation.z = orientation[2]
        msg.pose.orientation.w = orientation[3]
    return msg


def display_poses(poses, repeat_publish=5, display_topic="/poses"):
    posearr = PoseArray()
    posearr.header.frame_id = "world"
    for pose in poses:
        if len(pose.shape) == 2:
            # convert from transform
            posearr.poses.append(homogeneous_to_pose(pose))
        else:
            posearr.poses.append(array_to_pose(pose))
    pub = rospy.Publisher(display_topic, PoseArray, queue_size=1)
    if not wait_for_publisher(pub):
        rospy.logwarn(
            "Could not display pose array in rviz. Is rviz running and is a "
            "PoseArray instance subscribed to '{}'?".format(display_topic)
        )
        return False
    else:
        for _ in range(repeat_publish):
            pub.publish(posearr)
            rospy.sleep(0.05)
        return True


def display_pose(pose, repeat_publish=5, display_topic="/pose"):
    posemsg = PoseStamped()
    posemsg.header.frame_id = "world"
    if len(pose.shape) == 2:
        # convert from transform
        posemsg.pose = homogeneous_to_pose(pose)
    else:
        posemsg.pose = array_to_pose(pose)
    pub = rospy.Publisher(display_topic, PoseStamped, queue_size=1)
    if not wait_for_publisher(pub):
        rospy.logwarn(
            "Could not display pose msg in rviz. Is rviz running and is a "
            "pose instance subscribed to '{}'?".format(display_topic)
        )
        return False
    else:
        for _ in range(repeat_publish):
            pub.publish(posemsg)
            rospy.sleep(0.05)
        return True


def get_marker_msg(
    position=[0, 0, 0],
    orientation=[0, 0, 0, 1],
    pose=None,
    scale=[1, 1, 1],
    shape="cube",
    color="blue",
    alpha=1,
    marker_id=1,
    frame_id="world",
    text="",
    mesh_resource="",
    use_embedded_materials=False,
    points=[],
):
    marker = Marker()
    marker.id = marker_id
    if shape == "cube":
        marker.type = Marker.CUBE
    elif shape == "sphere":
        marker.type = Marker.SPHERE
    elif shape == "arrow":
        marker.type = Marker.ARROW
        marker.points = points
    elif shape == "text":
        marker.type = Marker.TEXT_VIEW_FACING
        marker.text = text
    elif shape == "mesh":
        marker.type = Marker.MESH_RESOURCE
        marker.mesh_resource = mesh_resource
        marker.mesh_use_embedded_materials = use_embedded_materials
    else:
        raise ValueError(f"Unknown shape for marker: {shape}")
    marker.action = Marker.MODIFY
    if pose is not None:
        marker.pose = pose
    else:
        marker.pose.position.x = position[0]
        marker.pose.position.y = position[1]
        marker.pose.position.z = position[2]
        marker.pose.orientation.x = orientation[0]
        marker.pose.orientation.y = orientation[1]
        marker.pose.orientation.z = orientation[2]
        marker.pose.orientation.w = orientation[3]
    marker.header.frame_id = frame_id
    marker.scale.x = scale[0]
    marker.scale.y = scale[1]
    marker.scale.z = scale[2]
    marker.color = get_color(color, alpha)
    return marker


def get_imarker_msg(
    position=[0, 0, 0],
    orientation=[0, 0, 0, 1],
    pose=None,
    scale=[1, 1, 1],
    shape="cube",
    color="blue",
    alpha=1,
    marker_id=1,
    frame_id="world",
    mesh_resource="",
    imarker_scale=0.3,
    imarker_name="",
    imarker_desc="",
    imarker_move_x=True,
    imarker_move_y=True,
    imarker_move_z=True,
    imarker_rotate_x=True,
    imarker_rotate_y=True,
    imarker_rotate_z=True,
):
    marker = get_marker_msg(
        position=position,
        orientation=orientation,
        pose=pose,
        scale=scale,
        shape=shape,
        color=color,
        alpha=alpha,
        marker_id=marker_id,
        frame_id=frame_id,
        mesh_resource=mesh_resource,
    )

    imarker = InteractiveMarker()
    imarker.header.frame_id = frame_id
    imarker.scale = imarker_scale
    imarker.name = imarker_name
    imarker.pose = marker.pose
    imarker.description = imarker_desc

    # Add the marker
    control = InteractiveMarkerControl()
    control.always_visible = True
    control.markers.append(marker)
    imarker.controls.append(control)

    # Add the controls
    if imarker_move_x:
        control = InteractiveMarkerControl()
        control.orientation.w = 0.70710678
        control.orientation.x = 0.70710678
        control.orientation.y = 0
        control.orientation.z = 0
        control.name = "move_x"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        imarker.controls.append(control)
    if imarker_rotate_x:
        control = InteractiveMarkerControl()
        control.orientation.w = 0.70710678
        control.orientation.x = 0.70710678
        control.orientation.y = 0
        control.orientation.z = 0
        control.name = "rotate_x"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        imarker.controls.append(control)
    if imarker_move_y:
        control = InteractiveMarkerControl()
        control.orientation.w = 0.70710678
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 0.70710678
        control.name = "move_y"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        imarker.controls.append(control)
    if imarker_rotate_y:
        control = InteractiveMarkerControl()
        control.orientation.w = 0.70710678
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 0.70710678
        control.name = "rotate_y"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        imarker.controls.append(control)
    if imarker_move_z:
        control = InteractiveMarkerControl()
        control.orientation.w = 0.70710678
        control.orientation.x = 0
        control.orientation.y = 0.70710678
        control.orientation.z = 0
        control.name = "move_z"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        imarker.controls.append(control)
    if imarker_rotate_z:
        control = InteractiveMarkerControl()
        control.orientation.w = 0.70710678
        control.orientation.x = 0
        control.orientation.y = 0.70710678
        control.orientation.z = 0
        control.name = "rotate_z"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        imarker.controls.append(control)
    return imarker


def publish_msg(msg, publisher, repeat_num=1, pause_time=0.05):
    for _ in range(repeat_num):
        if getattr(msg, "header", None) is not None:
            msg.header.stamp = rospy.Time.now()
        publisher.publish(msg)
        if _ < (repeat_num - 1):
            rospy.sleep(pause_time)


def publish_tf_msg(msg, publisher):
    for tf_stamped in msg.transforms:
        tf_stamped.header.stamp = rospy.Time.now()
        publisher.publish(msg)


def publish_marker_msg(msg, publisher):
    # for marker in msg.markers:
    #     marker.header.stamp = rospy.Time.now()
    publisher.publish(msg)


def interpolate_joint_trajectory(nominal_traj, dt=None):
    if len(nominal_traj.points) <= 1:
        raise ValueError("Trajectory must have at least 2 points to interpolate")
    if dt is None:
        # Compute dt that was used to generate the trajectory
        dt = (
            nominal_traj.points[-1].time_from_start.to_sec()
            - nominal_traj.points[-2].time_from_start.to_sec()
        )
    if dt <= 0 or dt > 1:
        # Want to be conservative here since this will go on the real robot
        raise ValueError(f"Maybe a bad dt value?: {dt}")
    duration = nominal_traj.points[-1].time_from_start.to_sec()
    old_time = np.linspace(0, duration, len(nominal_traj.points))
    new_time = np.linspace(0, duration, int(duration / float(dt)))

    old_pos = np.stack([p.positions for p in nominal_traj.points])
    n_dims = old_pos.shape[-1]
    new_pos = np.stack(
        [np.interp(new_time, old_time, old_pos[:, i]) for i in range(n_dims)], axis=-1
    )
    if nominal_traj.points[0].velocities:
        old_vel = np.stack([p.velocities for p in nominal_traj.points])
        new_vel = np.stack(
            [np.interp(new_time, old_time, old_vel[:, i]) for i in range(n_dims)],
            axis=-1,
        )
    else:
        new_vel = None
    if nominal_traj.points[0].accelerations:
        old_acc = np.stack([p.accelerations for p in nominal_traj.points])
        new_acc = np.stack(
            [np.interp(new_time, old_time, old_acc[:, i]) for i in range(n_dims)],
            axis=-1,
        )
    else:
        new_acc = None

    new_traj = JointTrajectory()
    new_traj.joint_names = nominal_traj.joint_names
    for t in range(len(new_time)):
        point = JointTrajectoryPoint()
        point.time_from_start = rospy.Duration(new_time[t])
        point.positions = new_pos[t, :]
        if new_vel is not None:
            point.velocities = new_vel[t, :]
        if new_acc is not None:
            point.accelerations = new_acc[t, :]
        new_traj.points.append(point)
    return new_traj


def plot_joint_trajectory(traj):
    duration = traj.points[-1].time_from_start.to_sec()
    time = np.linspace(0, duration, len(traj.points))
    pos = np.stack([p.positions for p in traj.points])
    vel = np.stack([p.velocities for p in traj.points])
    acc = np.stack([p.accelerations for p in traj.points])
    fig, axes = plt.subplots(3, 1)
    fig.set_size_inches(4, 12)
    axes[0].plot(time, pos)
    axes[1].plot(time, vel)
    axes[2].plot(time, acc)
    plt.tight_layout()
    plt.show()


def wait_for_publisher(pub, timeout=5.0):
    """
    Waits for a publisher to have subscribers.

    This is important when you create a publisher and then immediately try
    to publish a message, the timing can work out that no one subscribed in
    the time it took to initialize the publisher and publish the message,
    so your message can get lost. This ensures someone will receive the messgae.
    """
    start = time()
    while pub.get_num_connections() == 0:
        rospy.sleep(0.1)
        if time() - start > timeout:
            return False
    return True


def display_rviz_joint_position(
    joint_positions,
    robot_name="iiwa",
    joint_names=[
        "iiwa_joint_1",
        "iiwa_joint_2",
        "iiwa_joint_3",
        "iiwa_joint_4",
        "iiwa_joint_5",
        "iiwa_joint_6",
        "iiwa_joint_7",
    ],
    repeat_publish=5,
    display_topic="/joint_position",
):
    joint_state = JointState()
    joint_state.name = joint_names
    joint_state.position = joint_positions.tolist()
    joint_state.velocity = (joint_positions * 0).tolist()
    times = np.linspace(0, 2, 2)
    traj = JointTrajectory()
    traj.joint_names = joint_names
    point = JointTrajectoryPoint()
    point.positions = joint_state.position
    point.velocities = joint_state.velocity
    point.accelerations = joint_state.velocity
    point.time_from_start = rospy.Duration(times[0])
    traj.points.append(point)
    point = copy.deepcopy(point)
    point.time_from_start = rospy.Duration(times[1])
    traj.points.append(point)
    worked = display_rviz_trajectory(
        traj, joint_state, robot_name, repeat_publish=repeat_publish, display_topic=display_topic
    )
    return worked


def display_rviz_trajectory(
    traj, joint_state, model_id, display_topic="/display_trajectory", repeat_publish=1, loginfo=True
):
    robot_traj = RobotTrajectory()
    robot_traj.joint_trajectory = traj
    robot_state = RobotState()
    robot_state.joint_state = joint_state
    display_traj = DisplayTrajectory()
    display_traj.model_id = model_id
    display_traj.trajectory = [robot_traj]
    display_traj.trajectory_start = robot_state

    pub = rospy.Publisher(display_topic, DisplayTrajectory, queue_size=1)
    if loginfo:
        rospy.loginfo("Initializing trajectory display publisher...")
    if not wait_for_publisher(pub):
        if loginfo:
            rospy.logwarn(
                "Could not display trajectory in rviz. Is rviz running and is a "
                "Trajectory instance subscribed to '{}'?".format(display_topic)
            )
        return False
    else:
        for _ in range(repeat_publish):
            pub.publish(display_traj)
            rospy.sleep(0.05)
        if loginfo:
            rospy.loginfo("Trajectory is displayed in rviz")
        return True


def display_rviz_mesh(
    mesh_resource,
    pose_stmp,
    display_topic="/display_mesh",
    color="springgreen",
    alpha=1,
):
    position = [
        pose_stmp.pose.position.x,
        pose_stmp.pose.position.y,
        pose_stmp.pose.position.z,
    ]
    orientation = [
        pose_stmp.pose.orientation.x,
        pose_stmp.pose.orientation.y,
        pose_stmp.pose.orientation.z,
        pose_stmp.pose.orientation.w,
    ]
    marker = get_marker_msg(
        position,
        orientation,
        shape="mesh",
        color=color,
        alpha=alpha,
        frame_id=pose_stmp.header.frame_id,
        mesh_resource=mesh_resource,
    )

    pub = rospy.Publisher(display_topic, Marker, queue_size=1)
    rospy.loginfo("Initializing mesh display publisher...")
    if not wait_for_publisher(pub):
        rospy.logwarn(
            "Could not display mesh in rviz. Is rviz running and is a "
            "Marker instance subscribed to '{}'?".format(display_topic)
        )
    else:
        pub.publish(marker)
        rospy.loginfo("Mesh is displayed in rviz")


def display_rviz_points(points, topic="points", frame="world", color=[0, 0, 0]):
    """
    Publish the sparse grid as a point_cloud over ROS topic.

    Params:
    points: (N, 3) array of the points to publish
    topic: (str) of the topic to publish to
    frame: (str) frame from which to publish [default is world]

    Output:
    None - Publish ros topic over
    """
    pcl_pub_ = rospy.Publisher(topic, PointCloud, queue_size=10)
    point_cloud_ = PointCloud()
    point_cloud_.header = Header()
    point_cloud_.header.stamp = rospy.Time.now()
    point_cloud_.header.frame_id = frame

    point_cloud_.channels.append(ChannelFloat32())
    point_cloud_.channels[0].name = "r"
    point_cloud_.channels.append(ChannelFloat32())
    point_cloud_.channels[1].name = "g"
    point_cloud_.channels.append(ChannelFloat32())
    point_cloud_.channels[2].name = "b"

    for i in range(points.shape[0]):
        point_cloud_.points.append(Point32(points[i][0], points[i][1], points[i][2]))
        if len(color) == len(points):
            point_cloud_.channels[0].values.append(int(float(color[i][0]) * 255))
            point_cloud_.channels[1].values.append(int(float(color[i][1]) * 255))
            point_cloud_.channels[2].values.append(int(float(color[i][2]) * 255))
        else:
            point_cloud_.channels[0].values.append(int(float(color[0]) * 255))
            point_cloud_.channels[1].values.append(int(float(color[1]) * 255))
            point_cloud_.channels[2].values.append(int(float(color[2]) * 255))
    rate = rospy.Rate(20)
    for i in range(10):
        pcl_pub_.publish(point_cloud_)
        rate.sleep()
    return


def display_rviz_image(rgb_img, topic="rgb"):
    """
    Publish the rgb image.

    Params:
    rgb_img: (H, W, 3) numpy array of image values
    topic: (str) of the topic to publish to

    Output:
    None - Publish ros topic over
    """
    pub_ = rospy.Publisher(topic, Image, queue_size=10)
    rgb_msg = rgb_to_msg(rgb_img)
    rate = rospy.Rate(20)
    for i in range(10):
        pub_.publish(rgb_msg)
        rate.sleep()
    return


def call_service(req, service_name, service_type):
    service_call = rospy.ServiceProxy(service_name, service_type)
    resp = None
    try:
        resp = service_call(req)
        success = resp.success if hasattr(resp, "success") else True
    except rospy.ServiceException as e:
        rospy.logerr("Service call failed: {}".format(e))
        success = False
    return resp, success


def get_path(pkg_name):
    return rospack.get_path(pkg_name)


def resolve_ros_package_path(path):
    if path.startswith("package://"):
        path = path.replace("package://", "")
        pkg_path = get_path(path.split("/")[0])
        rel_path = "/".join(path.split("/")[1:])
        path = os.path.join(pkg_path, rel_path)
        return path
    elif path.startswith("/"):
        return path
    else:
        print("No ROS path to resolve. Expected string to start with 'package://'")


def get_mesh_filename_from_urdf(urdf_path, collision=False):
    """
    Extracts mesh filename from a URDF file.

    TODO can add option for looking for specific link, right now assuming there
    is only one link in the urdf

    TODO can add option to resolve ROS path or keep relative, right now relative
    """
    if urdf_path.startswith("package://"):
        urdf_path = resolve_ros_package_path(urdf_path)
    file_util.check_path_exists(urdf_path, "URDF file")

    xml = ET.parse(urdf_path)
    if collision:
        node = xml.find("link").find("collision")
    else:
        node = xml.find("link").find("visual")
    mesh_filename = node.find("geometry").find("mesh").attrib["filename"]
    # TODO assumes relative path to URDF, can make more general
    mesh_filename = os.path.join(os.path.dirname(urdf_path), mesh_filename)

    return mesh_filename


def tensor2rosTraj(traj, dt, max_vel_factor):

    from moveit_msgs.msg import RobotTrajectory
    from trajectory_msgs.msg import JointTrajectoryPoint
    from moveit_interface.srv import GetPlanResponse

    # see ~/isaac_ws/src/ll4ma_moveit/moveit_interface/srv/GetPlan.srv
    rostraj = GetPlanResponse()

    # probably unnecessary to define
    # see http://docs.ros.org/en/lunar/api/moveit_msgs/html/msg/RobotState.html
    # rostraj.start_state = RobotState()

    # see http://docs.ros.org/en/api/moveit_msgs/html/msg/RobotTrajectory.html
    rostraj.trajectory = RobotTrajectory()
    rostraj.trajectory.joint_trajectory.header.stamp = rospy.Time.now()
    velocs, accels = positions2velocities(traj), positions2accelerations(traj)
    # if (velocs > max_vel_factor).any():
    #     import pdb; pdb.set_trace()
    for ti, (pos, vel, acc) in enumerate(zip(traj.tolist(), velocs.tolist(), accels.tolist())):
        # see http://docs.ros.org/en/api/trajectory_msgs/html/msg/JointTrajectoryPoint.html
        pt = JointTrajectoryPoint()
        pt.positions = pos
        pt.velocities = vel
        pt.accelerations = acc
        # pt.effort = asdf
        pt.time_from_start = rospy.Duration(dt * ti)
        rostraj.trajectory.joint_trajectory.points.append(pt)
    return rostraj


def positions2velocities(traj):
    velocs = torch.zeros_like(traj)
    # velocs[0] = 0*traj[0] # initial velocity is 0
    velocs[1:-1] = traj[1:-1] - traj[0:-2]
    velocs[-1] = traj[-1] - traj[-2]
    return velocs


def positions2accelerations(traj):
    accels = torch.zeros_like(traj)
    accels[0] = traj[1] - traj[0]  # assumes starting velocity is 0
    accels[1:-1] = traj[2:] - 2 * traj[1:-1] + traj[:-2]
    accels[-1] = traj[-1] - traj[-2]  # assumes ending velocity is 0
    return accels


def invertTransform(trans):
    R = trans[:3, :3]
    t = trans[:3, 3]
    trans[:3, 3] = -R.T @ t
    trans[:3, :3] = R.T
    return trans


def pointcloud2_msg_to_numpy(pc_msg):
    pc2_arr = pc2.pointcloud2_to_array(pc_msg)
    pc2_arr = pc2.split_rgb_field(pc2_arr)
    points = np.stack((pc2_arr["x"], pc2_arr["y"], pc2_arr["z"])).T
    colors = np.stack((pc2_arr["r"], pc2_arr["g"], pc2_arr["b"])).T
    colors = colors.astype(np.float64) / 255.0
    return points, colors


def calibrate_iiwa_hand():
    service_name = "/reflex_takktile2/calibrate_fingers"
    rospy.wait_for_service(service_name)
    try:
        calibrate = rospy.ServiceProxy(service_name, std_srvs.Empty)
        calibrate()
        return True
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)
        return False
