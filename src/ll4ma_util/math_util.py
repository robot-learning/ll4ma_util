import torch
import numpy as np
from pyquaternion import Quaternion
from scipy.spatial.transform import Rotation

try:
    from tf.transformations import euler_matrix, euler_from_matrix

    def euler_to_homogeneous(euler_pose):
        pose = euler_pose
        if torch.is_tensor(euler_pose):
            pose = euler_pose.cpu().numpy()
        if len(pose.shape) == 1:
            pose = pose.reshape(1, -1)
        T = np.stack([euler_matrix(*pose[i, 3:]) for i in range(pose.shape[0])])
        T[:, :3, 3] = pose[:, :3]
        if torch.is_tensor(euler_pose):
            T = torch.tensor(T).to(euler_pose)
        return T

    def homogeneous_to_euler(matrix):
        mat = matrix
        if torch.is_tensor(matrix):
            mat = matrix.cpu().numpy()
        if len(matrix.shape) < 3:
            mat = mat.reshape(1, *matrix.shape)
        rot = np.stack([euler_from_matrix(mat[i]) for i in range(mat.shape[0])])
        pos = mat[:, :3, 3]
        euler = np.concatenate((pos, rot), -1)
        if torch.is_tensor(matrix):
            euler = torch.tensor(euler).to(matrix)
        if len(matrix.shape) < 3:
            euler = euler.squeeze()
        return euler

except ModuleNotFoundError:
    pass


def rotation_to_quat(R, normalize=False):
    istorch = torch.is_tensor(R)
    if istorch:
        device = R.device
        R = R.cpu().numpy()
    if normalize:
        # If R is not exactly a rotation matrix, we can leverage scipy to make it so
        R = Rotation.from_matrix(R).as_matrix()
    T = np.eye(4)
    T[:3, :3] = R
    quat = homogeneous_to_quat(T)
    if istorch:
        return torch.tensor(quat, device=device)
    return quat


def rotation_to_homogeneous(R):
    T = np.eye(4)
    T[:3, :3] = R
    return T


def homogeneous_to_position(T):
    if torch.is_tensor(T) and T.dim() == 3:
        p = T[:, :3, 3]  # Accounts for batch dim
    else:
        p = T[:3, 3]
    return p


def homogeneous_to_rotation(T):
    if torch.is_tensor(T) and T.dim() == 3:
        R = T[:, :3, :3]  # Accounts for batch dim
    else:
        R = T[:3, :3]
    return R


def homogeneous_to_quat(T):
    """
    Converts rotation matrix from homogeneous TF matrix to quaternion.
    """
    Tmat = T[:3, :3].cpu().numpy() if torch.is_tensor(T) else T[:3, :3]
    # q = Quaternion(matrix=Tmat[:3, :3])
    q = Rotation.from_matrix(Tmat[:3, :3]).as_quat()
    # Need to switch to x, y, z, w
    # q = (
    #     torch.tensor([q.x, q.y, q.z, q.w]).to(T)
    #     if torch.is_tensor(T)
    #     else np.array([q.x, q.y, q.z, q.w])
    # )
    if torch.is_tensor(T):
        q = torch.tensor(q).to(T)
    return q


def orthoganalize_rotation(matrix):
    a1, a2, a3 = matrix[:3, :3]
    b1 = a1 / np.linalg.norm(a1)
    b2 = a2 - b1 @ a2 * b1
    b2 = b2 / np.linalg.norm(b2)
    b3 = np.cross(b1, b2)
    return np.concatenate((b1, b2, b3)).reshape(3, 3)


def homogeneous_to_pose(T):
    """
    Converts homogeneous TF matrix to a 3D position and quaternion.
    """
    return homogeneous_to_position(T), homogeneous_to_quat(T)


def quat_to_homogeneous(q):
    """
    Converts quaternion to homogeneous TF matrix. Assumes (x, y, z, w) quaternion input.
    """
    if torch.is_tensor(q):
        qs = q.cpu().numpy()
    else:
        qs = q
    T = Quaternion(qs[3], qs[0], qs[1], qs[2]).transformation_matrix  # Quaternion is (w, x, y, z)
    if torch.is_tensor(q):
        T = torch.tensor(T).to(q)
    return T


def quat_to_rotation(q):
    """
    Converts quaternion to rotation matrix. Assumes (x, y, z, w) quaternion input.
    """
    return quat_to_homogeneous(q)[:3, :3]


def pose_to_homogeneous(p, q=None, R=None):
    """
    Converts position and orientation to a homogeneous TF matrix.
    Supports multiple orientation representations.

    Args:
        p: 3D Position of shape (3,)
        q: Quaternion (xyzw) orientation of shape (4,)
        R: Rotation matrix of shape (3,3)

    Note only specify one orientation representation.
    """
    if q is not None:
        T = quat_to_homogeneous(q)
    elif R is not None:
        T = rotation_to_homogeneous(R)
    else:
        raise ValueError("Must specify one of the orientation representations")
    T[:3, 3] = p
    return T


def homogeneous_inverse(T):
    """
    Computes inverse of homogeneous TF matrix.
    """
    if torch.is_tensor(T) and T.dim() == 3:
        # Handle batch
        new_T = torch.eye(4).unsqueeze(0).repeat(T.size(0), 1, 1).to(T)
        R_trans = T[:, :3, :3].transpose(1, 2)
        new_T[:, :3, :3] = R_trans
        new_T[:, :3, 3] = -torch.bmm(R_trans, T[:, :3, 3].unsqueeze(-1)).squeeze(-1)
    else:
        new_T = torch.eye(4).to(T) if torch.is_tensor(T) else np.eye(4)
        new_T[:3, :3] = T[:3, :3].T
        new_T[:3, 3] = -(new_T[:3, :3] @ T[:3, 3])
    return new_T


def random_quat():
    q = Quaternion.random()
    return np.array([q.x, q.y, q.z, q.w])


def random_rotation():
    return quat_to_rotation(random_quat())


def uniform_random_planar_pose(
    min_pos, max_pos, sample_axis=[0, 0, 1], min_angle=-np.pi, max_angle=np.pi
):
    pos_ranges = zip(min_pos, max_pos)
    pos = np.array([np.random.uniform(*pos_range) for pos_range in pos_ranges])
    angle = np.random.uniform(min_angle, max_angle)
    q = Quaternion(axis=sample_axis, angle=angle)
    quat = np.array([q.x, q.y, q.z, q.w])
    return pos, quat


def gaussian_random_planar_poses(
    pos_mean=[0.0, 0.0, 0.0],
    pos_std=[0.1, 0.1, 0.1],
    angle_std=1.0,
    sample_axis=[0, 0, 1],
    n_samples=1,
    rot_return="matrix",
):
    """
    Computes random 3D poses where positions are sampled from a Gaussian, and
    an orientation angle about a sample axis is sampled from a Gaussian.

    Args:
        pos_mean: Mean 3D position
        pos_std: Standard deviation for 3D position
        angle_std: Standard deviation of angle about sample axis
        sample_axis: Sample axis for randomizing orientation
        n_samples: Number of samples to compute
        rot_return: Orientation representation to return, one of 'quat', 'matrix',
                    'euler', or 'rotvec'.
    """
    pos_samples = np.random.normal(pos_mean, pos_std, size=(n_samples, 3))
    angle_samples = np.expand_dims(np.random.normal(0.0, angle_std, size=(n_samples,)), -1)
    axes = np.expand_dims(np.array(sample_axis), 0).repeat(n_samples, axis=0)
    rots = Rotation.from_rotvec(angle_samples * axes)
    if rot_return == "quat":
        rot_samples = rots.as_quat()
    elif rot_return == "matrix":
        rot_samples = rots.as_matrix()
    elif rot_return == "euler":
        rot_samples = rots.as_euler()
    elif rot_return == "rotvec":
        rot_samples = rots.as_rotvec()
    else:
        raise ValueError(f"Unknown rotation return type: {rot_return}")
    return pos_samples, rot_samples


def get_x_axis_rotation(theta):
    lib = torch if torch.is_tensor(theta) else np
    costheta = lib.cos(theta)
    sintheta = lib.sin(theta)
    rot = lib.zeros((*theta.shape, 3, 3))
    if torch.is_tensor(theta):
        rot = rot.to(theta)
    else:
        rot = rot.astype(theta.dtype)
    rot[..., 0, 0] = 1
    rot[..., 1, 1] = costheta
    rot[..., 2, 1] = sintheta
    rot[..., 1, 2] = -sintheta
    rot[..., 2, 2] = costheta
    return rot


def get_y_axis_rotation(theta):
    lib = torch if torch.is_tensor(theta) else np
    costheta = lib.cos(theta)
    sintheta = lib.sin(theta)
    rot = lib.zeros((*theta.shape, 3, 3))
    if torch.is_tensor(theta):
        rot = rot.to(theta)
    else:
        rot = rot.astype(theta.dtype)
    rot[..., 0, 0] = costheta
    rot[..., 0, 2] = sintheta
    rot[..., 2, 0] = -sintheta
    rot[..., 2, 2] = costheta
    rot[..., 1, 1] = 1
    return rot


def get_z_axis_rotation(theta):
    lib = torch if torch.is_tensor(theta) else np
    costheta = lib.cos(theta)
    sintheta = lib.sin(theta)
    rot = lib.zeros((*theta.shape, 3, 3))
    if torch.is_tensor(theta):
        rot = rot.to(theta)
    else:
        rot = rot.astype(theta.dtype)
    rot[..., 0, 0] = costheta
    rot[..., 1, 0] = sintheta
    rot[..., 0, 1] = -sintheta
    rot[..., 1, 1] = costheta
    rot[..., 2, 2] = 1
    return rot


def construct_rotation_matrix(x=None, y=None, z=None, normalize=True):
    """
    Constructs a (right-handed) rotation matrix given the individual
    axes. Must specify at least two axes, and the third will be computed
    as a cross product of the others.

    Args:
        x (ndarray): X-axis of shape (3,)
        y (ndarray): Y-axis of shape (3,)
        z (ndarray): Z-axis of shape (3,)
        normalize (bool): Inputs are normalized to unit length if True. If you know
                          your vectors are unit already you can set False.
    """
    if normalize:
        x = x if x is None else x / np.linalg.norm(x)
        y = y if y is None else y / np.linalg.norm(y)
        z = z if z is None else z / np.linalg.norm(z)

    if x is None:
        if y is None or z is None:
            raise ValueError("Must specify at least two axes to construct rotation matrix")
        x = np.cross(y, z)
        x /= np.linalg.norm(x)
    elif y is None:
        if x is None or z is None:
            raise ValueError("Must specify at least two axes to construct rotation matrix")
        y = np.cross(z, x)
        y /= np.linalg.norm(y)
    elif z is None:
        if x is None or y is None:
            raise ValueError("Must specify at least two axes to construct rotation matrix")
        z = np.cross(x, y)
        z /= np.linalg.norm(z)

    R = np.eye(3)
    R[:, 0] = x
    R[:, 1] = y
    R[:, 2] = z

    # Sometimes it's close but apparently not perfect which is problematic when
    # you try to do quaternion conversions, so leveraging scipy's normalization
    R = Rotation.from_matrix(R).as_matrix()

    return R


def quat_mul(a, b):
    x1, y1, z1, w1 = a
    x2, y2, z2, w2 = b
    ww = (z1 + x1) * (x2 + y2)
    yy = (w1 - y1) * (w2 + z2)
    zz = (w1 + y1) * (w2 - z2)
    xx = ww + yy + zz
    qq = 0.5 * (xx + (z1 - x1) * (x2 - y2))
    w = qq - ww + (z1 - y1) * (y2 - z2)
    x = qq - xx + (x1 + w1) * (x2 + w2)
    y = qq - yy + (w1 - x1) * (y2 + z2)
    z = qq - zz + (z1 + y1) * (w2 - x2)

    if torch.is_tensor(a):
        quat = torch.stack([x, y, z, w], dim=-1).view(a.shape)
    else:
        quat = np.array([x, y, z, w])

    return quat


def geodesic_error(R1, R2):
    return np.arccos(np.clip((np.trace(np.dot(R1, R2.T)) - 1.0) / 2.0, -1, 1))


def rotation_matrix_log(T):
    """
    Lynch And Park Equation 3.53 / 3.54
    with minor check to ensure the matrix is sufficienty close to valid rotation matrix
    if not, row normalize
    """

    if T.dim() == 2:
        trace = torch.trace(T)
        T_T = T.T
    else:
        # T is batch,3,3
        trace = T.diagonal(offset=0, dim1=-1, dim2=-2).sum(-1)
        T_T = T.transpose(1, 2)

    # make the value just tiny bit smaller to avoid acos limits
    if torch.any(torch.abs(trace - 1) > 2.0):
        T = torch.nn.functional.normalize(T, p=2, dim=1)

    phi = torch.arccos(0.5 * (trace - 1) * (1 - 1e-10)).unsqueeze(-1).unsqueeze(-1)

    skew_symmetric_unit_rotation = (1 / (2 * torch.sin(phi))) * (T - T_T)

    # cleary this variable name is ridiculous...
    return skew_symmetric_unit_rotation, phi


def adjoint_matrix(T, angle_first=False):
    """
    Takes a list of 4x4 transformation matrix and converts it to a 6x6 adjoint matrix
    See Lynch and Park 3.83 (assumes the angular and positional velocities are switched)
    """
    zeros_fn = torch.zeros if torch.is_tensor(T) else np.zeros
    sshape = T.shape
    if len(sshape) == 2:
        T = T.unsqueeze(0)

    adj = zeros_fn((T.shape[0], 6, 6), dtype=T.dtype)
    if torch.is_tensor(T):
        adj = adj.to(T.device)
    if angle_first:
        R = T[:, :3, :3]
        p = T[:, :3, 3]
        adj[:, :3, :3] = R
        adj[:, 3:, 3:] = R
        adj[:, 3:, :3] = skew_symmetric(p) @ R
    else:
        R = T[:, :3, :3]
        p = T[:, :3, 3]
        adj[:, :3, :3] = R
        adj[:, :3, 3:] = skew_symmetric(p) @ R
        adj[:, 3:, 3:] = R

    if len(sshape) == 2:
        adj = adj.squeeze(0)
    return adj


def skew_symmetric(pos):
    """
    Takes a position vector and coverts it to a 3x3 skew-symmetric matrix
    See Lynch and Park 3.30
    """
    zeros_fn = torch.zeros if torch.is_tensor(pos) else np.zeros
    sshape = pos.shape
    if len(sshape) == 1:
        pos = pos.unsqueeze(0)
    skew = zeros_fn((*pos.shape[:-1], 3, 3), dtype=pos.dtype)
    if torch.is_tensor(pos):
        skew = skew.to(pos.device)
    skew[..., 0, 1] = -pos[..., 2]
    skew[..., 0, 2] = pos[..., 1]
    skew[..., 1, 0] = pos[..., 2]
    skew[..., 1, 2] = -pos[..., 0]
    skew[..., 2, 0] = -pos[..., 1]
    skew[..., 2, 1] = pos[..., 0]
    if len(sshape) == 1:
        skew = skew.squeeze(0)
    return skew


def min_max_scale(x, min_x, max_x, a, b):
    """
    Scale x (which comes from range [min_x, max_x]) to be in range [a,b].

    See: https://en.wikipedia.org/wiki/Feature_scaling
    """
    return a + (((x - min_x) * (b - a)) / (max_x - min_x))


def apply_transform(points, transform):
    """
    points: (N, 3 or 4) set of points to transform
    transform: (4, 4) transform to apply
    Returns:
    transformed points: (N, 3)
    """
    copy = torch.clone if torch.is_tensor(points) else np.copy
    cat = torch.cat if torch.is_tensor(points) else np.concatenate
    points_1 = copy(points)
    if points.shape[1] == 3:
        points_1 = cat((points_1, points_1[:, 0:1] * 0 + 1), -1)
    assert transform.shape == (4, 4)
    trans_pts = points_1 @ transform.T
    if points.shape[1] == 3:
        return trans_pts[:, :3]
    return trans_pts

def batch_vector_similarity(a, b):
    """
    Outputs cosine similarity between two vector batches, useful for comparing gradients
    a, b: Inputs batch of vectors to compare (N, D) or (D,)

    Returns:
    Cosine similarity (N,): [-1, 1] - 0 - dissimilar,  1 similar, -1 similar but opposite direction
    """
    assert len(a.shape) in [1,2], 'a and (b) must be 1D or 2D'
    if len(a.shape) < 2:
        a = a.unsqueeze(0)
    N, D = a.shape
    if len(b.shape) < 2:
        b = b.unsqueeze(0)
    assert b.shape == (N, D), 'b must be same shape as a'
    assert type(a) == type(b), 'a and b must be same type'

    math_lib = np
    if isinstance(a, torch.Tensor):
        math_lib = torch

    al = math_lib.linalg.norm(a, dim=1).unsqueeze(1)
    bl = math_lib.linalg.norm(b, dim=1).unsqueeze(1)
    
    dir_res = ((a/al)*(b/bl)).sum(dim=1)
    dir_res[(al*bl).squeeze() == 0] = 0.0

    mag_res = al/bl
    mag_res[torch.logical_and(bl == 0, al == 0)] = 1
    return dir_res, mag_res


if __name__ == "__main__":
    R1 = random_rotation()
    R2 = random_rotation()

    print(geodesic_error(R1, R2))
