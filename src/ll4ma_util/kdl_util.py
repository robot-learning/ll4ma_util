import PyKDL as kdl
import torch
import numpy as np

from . import torch_util


def tokdlRot(R):
    Rin = R
    if torch.is_tensor(R):
        Rin = R.cpu()
    return kdl.Rotation(*Rin.flatten())


def tokdlFrame(T):
    Tin = T
    if torch.is_tensor(T):
        Tin = T.cpu()
    if Tin.shape == (4, 4):
        pos = Tin[:3, 3].flatten()
        ori = Tin[:3, :3].flatten()
        ori = kdl.Rotation(*ori)
    elif Tin.shape == (7,):
        pos = Tin[:3]
        ori = Tin[3:7]
        ori = kdl.Rotation.Quaternion(*ori)
    else:
        print("Input is invalid shape:", Tin.shape)
        raise Exception("Input is invalid shape: " + str(Tin.shape))
    return kdl.Frame(ori, kdl.Vector(*pos))


def fromkdl(v, vo):
    if isinstance(v, kdl.Frame):
        outen = torch.zeros((4, 4))
        for i in range(3):
            for j in range(4):
                outen[i, j] = v[i, j]
        outen[3, 3] = 1
        if torch.is_tensor(vo):
            return outen.to(vo)
        else:
            return outen.numpy()
    if torch.is_tensor(vo):
        return torch.tensor([*v]).to(vo).squeeze()
    else:
        return np.array([*v]).squeeze()


def rotation_log_matrix(R, R2=None):
    """
    Takes a Rotation matrix (or 2) and returns the axis-angle representation
      - R : (3,3) torch.tensor representation of rotation
      - R2 : (3,3) torch.tensor representation of rotation
    Returns:
      - angle : (,) float of same type as R
      - axis : (3,) array of same type as input R
    """
    if R2 is not None:
        R = R @ R2.T
    angle, axis = tokdlRot(R).GetRotAngle()
    axis = fromkdl(axis, R)
    return angle, axis


def orientation_twist(R, R2):
    err = kdl.diff(tokdlRot(R2), tokdlRot(R))
    err = fromkdl(err, R)
    return err


def orientation_error(R, R2, use_sqrt=False):
    """
    Takes two Rotation matrices and returns the angle between them as the error
      - R : (3,3) torch.tensor representation of rotation
      - R2 : (3,3) torch.tensor representation of rotation
    Returns:
      - error : (,) torch.tensor
    """
    return rotation_log_matrix(R, R2)[0]
    err = orientation_twist(R, R2)
    sqrt_fun = torch.sqrt if torch.is_tensor(R) else np.sqrt
    cost = err @ err
    if use_sqrt:
        cost = sqrt_fun(cost)
    return cost


def orientation_error_grad(R, R2):
    """
    Takes two Rotation matrices and returns the axis of the angle between
    them as the gradient of the error
      - R : (3,3) torch.tensor representation of rotation
      - R2 : (3,3) torch.tensor representation of rotation
    Returns:
      - gradient : (3,) torch.tensor
    """
    return orientation_twist(R, R2)


def get_twist(actual_T, target_T, pos_weight):
    t_err = kdl.diff(tokdlFrame(target_T), tokdlFrame(actual_T))
    t_err = fromkdl(t_err, actual_T)
    t_err[:3] = t_err[:3] * pos_weight
    return t_err


def apply_twist(trans, twist, pos_weight):
    Tframe = tokdlFrame(trans)
    twist[:3] = twist[:3] / pos_weight
    if torch.is_tensor(twist):
        twist = twist.cpu()
    newFrame = kdl.addDelta(Tframe, kdl.Twist(kdl.Vector(*twist[:3]), kdl.Vector(*twist[3:])))
    return fromkdl(newFrame, trans)


def pose_cost(x, actual_T, target_T, pos_weight):
    t_err = get_twist(actual_T, target_T, pos_weight)
    p_err = 0.5 * (t_err[:3] ** 2).sum()
    sqrt = torch.sqrt if torch.is_tensor(actual_T) else np.sqrt
    o_err = sqrt((t_err[3:] ** 2).sum())
    return p_err + o_err


def pose_grad(x, actual_T, target_T, pos_weight, jacobian, jacinv=None):
    t_err = get_twist(actual_T, target_T, pos_weight)
    sqrt = torch.sqrt if torch.is_tensor(actual_T) else np.sqrt
    norm = sqrt((t_err[3:] ** 2).sum())
    if norm > 0:
        t_err[3:] = t_err[3:] / norm
    # grad = t_err @ jacobian
    if jacinv is None:
        jacinv = jacobian_pseudoinverse(jacobian)
    grad = (jacinv @ t_err.reshape(6, 1)).reshape(-1)
    return grad


def jacobian_pseudoinverse(jacobian):
    if torch.is_tensor(jacobian):
        jac_pinv = torch_util.get_pinv(jacobian.unsqueeze(0), 1e-5).squeeze(0)
    else:
        jac_pinv = np.linalg.pinv(jacobian)
    return jac_pinv
