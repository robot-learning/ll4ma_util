import numpy as np
import torch
import matplotlib.pyplot as plt

try:
    import open3d as o3d
except Exception as e:
    print(e)

from ll4ma_util import math_util


def apply_transform(pc_data, transform):
    assert isinstance(pc_data, type(transform))
    T = transform
    if torch.is_tensor(pc_data):
        T = T.to(pc_data)
    pc_data[:, :3] = pc_data[:, :3] @ T[:3, :3].T + T[:3, 3:].T
    return pc_data


def get_pc_pose(input_pc_data, cam_vec=None, include_bounds=False, use_pca=False):
    if input_pc_data.shape[0] < 1:
        return None
    pc = input_pc_data.cpu().numpy() if torch.is_tensor(input_pc_data) else input_pc_data.copy()
    pc_maxs = pc[:, :3].max(0)
    pc_mins = pc[:, :3].min(0)
    pc_means = pc[:, :3].mean(0)
    bounds = pc_maxs - pc_mins
    pose = np.eye(4)
    pose[:2, 3] += pc_means[:2]
    if cam_vec is not None:
        norm_vec = pose[:2, 3] - cam_vec[:2]
        norm_vec = norm_vec / np.sqrt((norm_vec**2).sum())
        # pose[:2, 3] = pc_mins[:2] + bounds[:2] * norm_vec #* 0.9
        obj_offset = norm_vec * 0.02
        pose[:2, 3] += obj_offset
    pose[2, 3] = (pc_maxs[2] + pc_mins[2]) / 2
    if use_pca:
        center = pc_means
        pc_normed = pc[:, :3] - center.unsqueeze(0)
        U, S, V = torch.pca_lowrank(torch.tensor(pc_normed[:, :2]))
        wTpc = torch.eye(4).to(V)
        wTpc[:2, 0] = V[:, 0]
        wTpc[:3, 1] = torch.cross(wTpc[:3, 2], wTpc[:3, 0])
        if (torch.det(wTpc[:3, :3]) - 1).abs() > 1e-6:
            import pdb

            pdb.set_trace()  # noqa: E702
        pose[:3, :3] = wTpc[:3, :3]
    if torch.is_tensor(input_pc_data):
        pose = torch.tensor(pose).to(input_pc_data)
        bounds = torch.tensor(bounds).to(input_pc_data)
    if include_bounds:
        return pose, bounds
    return pose
    # pc_data = input_pc_data.cpu() if torch.is_tensor(input_pc_data) \
    #     else torch.tensor(input_pc_data)
    # center = pc_data[:, :3].mean(0)
    # pc_normed = pc_data[:, :3] - center.unsqueeze(0)
    # U, S, V = torch.pca_lowrank(pc_normed[:, :2])
    # wTpc = torch.eye(4).to(pc_normed)
    # wTpc[:2, 0] = V[:, 0]
    # wTpc[:3, 1] = torch.cross(wTpc[:3, 2], wTpc[:3, 0])
    # wTpc[:3, 3] = center
    # if (torch.det(wTpc[:3, :3]) - 1).abs() > 1e-6:
    #     import pdb; pdb.set_trace()  # noqa: E702
    # if torch.is_tensor(input_pc_data):
    #     return wTpc.to(input_pc_data)
    # return wTpc.numpy()


def center_pc_on_pose(pc_data, pose, inv_pose=None):
    if inv_pose is None:
        inv_pose = math_util.homogeneous_inverse(pose)
    # after defining the pc pose apply the inverse transform to put the point
    # cloud in its frame of origin
    if torch.is_tensor(pc_data):
        ipose = inv_pose.to(pc_data)
    else:
        ipose = inv_pose.astype(pc_data.dtype)
    pc_data[:, :3] = pc_data[:, :3] @ ipose[:3, :3].T + ipose[:3, 3:].T
    return pc_data


def get_pc_pose_and_center(pc_data):
    pc_pose = get_pc_pose(pc_data)
    pc_data = center_pc_on_pose(pc_data, pc_pose)
    return pc_pose, pc_data


def pc_from_indices(
    rgb,
    depth,
    height,
    width,
    projection_matrix,
    view_matrix=None,
    indices=None,
    segments=None,
    seg_id=None,
    minrc=(0, 0),
    depth_val=1000,
):
    """
    indices: (n, 2) array of indices that index each pixel in the image wanted in the point cloud
    minrc: (r, c) allows you to define the rgb and depth as sub images from an
        original image where the indices are indexed into the sub image but the
        minrc allows the index to go back to the og image for correct pojection
        calculation
    """
    if not torch.is_tensor(rgb):
        rgb = torch.tensor(rgb)
    if rgb.dtype != torch.float32:
        # assuming the input is uint8 in range [0, 255]
        # converting to floats in [0, 1]
        rgb = rgb.to(torch.float32) / 255
    if not torch.is_tensor(depth):
        depth = torch.tensor(depth)
    if not torch.is_tensor(projection_matrix):
        projection_matrix = torch.tensor(projection_matrix)
    if view_matrix is not None and not torch.is_tensor(view_matrix):
        view_matrix = torch.tensor(view_matrix)
    if indices is None:
        rows, cols = torch.meshgrid(torch.arange(height), torch.arange(width), indexing="ij")
        indices = torch.cat((rows.reshape(-1, 1), cols.reshape(-1, 1)), dim=-1)
    rows, cols = indices[:, 0], indices[:, 1]
    depths = depth[rows, cols].reshape(-1)
    indices = indices[depths != 0]
    rows, cols = indices[:, 0], indices[:, 1]
    depths = depth[rows, cols].reshape(-1)
    colors = rgb[rows, cols].reshape(-1, 3)

    fx = projection_matrix[0, 0]
    fy = projection_matrix[1, 1]
    cx = projection_matrix[0, 2]
    cy = projection_matrix[1, 2]

    zpos = depths / depth_val
    xpos = ((minrc[1] + cols - cx) / fx) * zpos
    ypos = ((minrc[0] + rows - cy) / fy) * zpos
    points = np.stack((xpos, ypos, zpos), -1)

    if view_matrix is not None:
        points = points @ view_matrix

    return points, colors


def image_xy_2_world_pt(
    rows, cols, depths, projection_matrix, worldTcamera=None, minrc=(0, 0), depth_val=1000
):
    fx = projection_matrix[0, 0]
    fy = projection_matrix[1, 1]
    cx = projection_matrix[0, 2]
    cy = projection_matrix[1, 2]

    zpos = depths / depth_val
    xpos = ((minrc[1] + cols - cx) / fx) * zpos
    ypos = ((minrc[0] + rows - cy) / fy) * zpos
    points = np.stack((xpos, ypos, zpos), -1)

    if worldTcamera is not None:
        points = apply_transform(points, worldTcamera).T[:3]

    return points


def world_pt_2_image_xy(w_pts, projection_matrix, cameraTworld):
    is_torch = torch.is_tensor(w_pts)
    if not is_torch:
        w_pts = torch.tensor(w_pts)
    if not torch.is_tensor(projection_matrix):
        projection_matrix = torch.tensor(projection_matrix)
    if not torch.is_tensor(cameraTworld):
        cameraTworld = torch.tensor(cameraTworld)

    fx = projection_matrix[0, 0]
    fy = projection_matrix[1, 1]
    cx = projection_matrix[0, 2]
    cy = projection_matrix[1, 2]

    xpos, ypos, zpos = apply_transform(w_pts.clone(), cameraTworld).T[:3]
    cols = cx + fx * xpos / zpos
    rows = cy + fy * ypos / zpos

    # xpos2 = ((cols - cx) / fx) * zpos
    # ypos2 = ((rows - cy) / fy) * zpos
    # assert torch.allclose(xpos2, xpos)
    # assert torch.allclose(ypos2, ypos)

    rows, cols = rows.round().to(torch.int), cols.round().to(torch.int)

    if not is_torch:
        rows = rows.numpy()
        cols = cols.numpy()

    return rows, cols


def pc_to_sparsevoxels(
    points, pc_dims, voxel_grid_dim=[26, 26, 26], voxel_grid_full_dim=[32, 32, 32], colors=None
):
    """
    Takes a set of points (expected to be centered around the origin) and
    returns a set of sparse voxel points (which can be used to fill in a grid)
    Params:
    - points: (N, 3) float np.array of point cloud points
    - pc_dims: (3,) int np.array dimensions of the point cloud
               (size of the object)
    - voxel_grid_dim: (3,) int np.array dimensions of the occupancy grid
    - voxel_grid_full_dim: (3,) int np.array dimensions of the sdf grid
                           (bigger to allow for sdf values to be calculated)
    - colors: (N, 3) float np.array of colors corresponding to the points
                     (default is None)
    Return:
    - voxel points: (M, 3) int np.array of voxel points (0 or 1)
    - voxel_grid_full_dim: (3,)
    - colors: (M, 3) float np.array of voxel points (0 or 1)
              (only if input colors are not None)
    """
    use_torch = torch.is_tensor(points)
    pmax = points.max(0)
    pmin = points.min(0)
    if use_torch:
        pmax = pmax[0]
        pmin = pmin[0]
        lib = torch
        voxel_grid_dim = torch.tensor(voxel_grid_dim).to(points)
        voxel_grid_full_dim = torch.tensor(voxel_grid_full_dim).to(points)
    else:
        lib = np
        voxel_grid_dim = np.array(voxel_grid_dim)
        voxel_grid_full_dim = np.array(voxel_grid_full_dim)
    voxel_trans_dim = (voxel_grid_full_dim - voxel_grid_dim) // 2

    center = (pmax + pmin) / 2
    points = points - center
    points = points / max(pc_dims)
    voxel_corner = -voxel_grid_dim / 2
    voxpts = points * voxel_grid_dim - voxel_corner
    voxpts = voxpts.round()
    in_rng = lib.logical_and((voxpts >= 0).all(1), (voxpts <= voxel_grid_dim).all(1))
    voxpts = voxpts[in_rng]
    if colors is not None:
        colors = colors[in_rng]

    voxpts = voxpts + voxel_trans_dim

    if use_torch:
        voxpts = voxpts.to(torch.int32)
        _, idx = np.unique(voxpts.cpu().numpy(), axis=0, return_index=True)
        voxel_grid_full_dim = voxel_grid_full_dim.cpu().long()
    else:
        voxpts = voxpts.astype(np.int32)
        _, idx = np.unique(voxpts, axis=0, return_index=True)
        voxel_grid_full_dim = voxel_grid_full_dim.astype(int)
    voxpts = voxpts[idx]
    if colors is not None:
        colors = colors[idx]
        return voxpts, voxel_grid_full_dim, colors
    return voxpts, voxel_grid_full_dim


def sparsevoxels_to_voxelgrid(sparse_pts, sparse_dims, colors=None):
    """
    Takes a set of sparse points (indices into a voxel grid) and the
    dimensions of the desired voxel grid, and outputs the full voxel grid.
    Params:
    - sparse_pts: (N, 3) int array
    - sparse_dims: (3,) int array of voxel dimensions
    - colors: (N, 3) float array of the voxel colors (default is None)
              if not None returns the color grid instead of a boolean
              occupancy grid
    Returns:
    - voxel_grid: (W, D, H) array of occupancy
        OR
    - voxel_grid: (W, D, H, 3) array of colors
    """
    if colors is not None:
        voxels = torch.zeros((*sparse_dims, 3))
        voxels[sparse_pts[:, 0], sparse_pts[:, 1], sparse_pts[:, 2]] += colors
    else:
        voxels = torch.zeros((*sparse_dims.cpu().long()), device=sparse_pts.device)
        voxels[sparse_pts[:, 0], sparse_pts[:, 1], sparse_pts[:, 2]] += 1
    return torch.clamp(voxels, 0, 1)


def get_sparse_voxels(xyz, object_bounds, color=None):
    sparse_rgb = None
    if color is None:
        sparse_voxels, sparse_dims = pc_to_sparsevoxels(xyz, object_bounds, colors=color)
    else:
        sparse_voxels, sparse_dims, sparse_rgb = pc_to_sparsevoxels(
            xyz, object_bounds, colors=color
        )
        if not torch.is_tensor(sparse_rgb):
            sparse_rgb = torch.tensor(sparse_rgb)
    if not torch.is_tensor(sparse_voxels):
        sparse_voxels = torch.tensor(sparse_voxels)
    sparse_voxels = sparse_voxels.long()
    return sparse_voxels, sparse_dims, sparse_rgb


def pc_to_voxelgrid(
    points, pc_dims, voxel_grid_dim=[26, 26, 26], voxel_grid_full_dim=[32, 32, 32], colors=None
):
    """
    Takes a set of points (expected to be centered around the origin) and
    returns a set of sparse voxel points (which can be used to fill in a grid)
    Params:
    - points: (N, 3) float np.array of point cloud points
    - pc_dims: (3,) int np.array dimensions of the point cloud
               (size of the object)
    - voxel_grid_dim: (3,) int np.array dimensions of the occupancy grid
    - voxel_grid_full_dim: (3,) int np.array dimensions of the sdf grid
                           (bigger to allow for sdf values to be calculated)
    - colors: (N, 3) float np.array of colors corresponding to the points
                     (default is None)
    Return:
    - voxel_grid: (W, D, H) array of occupancy
    """
    sparse_voxels, sparse_dims = pc_to_sparsevoxels(
        points,
        pc_dims,
        voxel_grid_dim=voxel_grid_dim,
        voxel_grid_full_dim=voxel_grid_full_dim,
        colors=colors,
    )
    return sparsevoxels_to_voxelgrid(sparse_voxels, sparse_dims)


def add_noise(points, colors=None, pose=None):
    """
    assumes the points given are centered around the origin unless the pose is given
    """
    num_noise = len(points) // 2
    noise = np.random.rand(num_noise, 3) - 0.5
    pmax = points.max(0)
    pmin = points.min(0)
    cat = np.concatenate
    if torch.is_tensor(points):
        noise = torch.tensor(noise).to(points)
        pmax = pmax[0]
        pmin = pmin[0]
        cat = torch.cat
    noise = noise * (pmax - pmin)
    if pose is None:
        pose = (pmax - pmin) / 2 + pmin
    noise = noise + pose[:3]
    noisy_pts = cat((points, noise), 0)
    if colors is not None:
        color_noise = noise * 0
        # Select the color of the nearest point in the point cloud
        for pi, pt in enumerate(noise):
            if torch.is_tensor(points):
                idx = torch.argmin(torch.sqrt(((pt - points) ** 2).sum(1)))
            else:
                idx = np.argmin(np.sqrt(((pt - points) ** 2).sum(1)))
            color_noise[pi] = colors[idx]
        noisy_colors = cat((colors, color_noise), 0)
        return noisy_pts, noisy_colors
    return noisy_pts


def display_voxels(sparse_pts=None, voxels=None):
    assert sparse_pts is None or voxels is None
    if voxels is None:
        sparse_pts, sparse_dims = sparse_pts
        voxels = sparsevoxels_to_voxelgrid(sparse_pts, sparse_dims)
    if torch.is_tensor(voxels):
        voxels = voxels.cpu().numpy()
    ax = plt.figure().add_subplot(projection="3d")
    ax.voxels(voxels, edgecolor="k")
    plt.show()
    return voxels, ax


def display_pc(points, colors=None):
    pcd = o3d.geometry.PointCloud()
    pts = points.cpu().numpy() if torch.is_tensor(points) else points
    pcd.points = o3d.utility.Vector3dVector(pts)
    if colors is not None:
        clr = colors.cpu().numpy() if torch.is_tensor(colors) else points
        pcd.colors = o3d.utility.Vector3dVector(clr)
    o3d.visualization.draw_geometries([pcd])


def display_rgbd(rgb, depth):
    rgb = o3d.geometry.Image(rgb)
    depth = o3d.geometry.Image(depth)
    rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(rgb, depth)
    o3d.visualization.draw_geometries([rgbd_image])


def display_rgb(rgb):
    rgb = o3d.geometry.Image(rgb)
    o3d.visualization.draw_geometries([rgb])


def to_o3d_pc(points, colors=None):
    pcd = o3d.geometry.PointCloud()
    pts = points.cpu().numpy() if torch.is_tensor(points) else points
    pcd.points = o3d.utility.Vector3dVector(pts[:, :3])
    if colors is not None or pts.shape[1] > 3:
        if colors is not None:
            clr = colors.cpu().numpy() if torch.is_tensor(colors) else points
        else:
            clr = pts[:, 3:6]
        if clr.dtype != np.float:
            # assuming the input is uint8 in range [0, 255]
            # converting to floats in [0, 1]
            clr = clr.astype(np.float) / 255
        pcd.colors = o3d.utility.Vector3dVector(clr)
    pcd.estimate_normals()
    return pcd


def o3d_to_np(pcd):
    pts = np.asarray(pcd.points)
    clr = np.asarray(pcd.colors)
    pc = pts
    if len(clr) == len(pts):
        pc = np.concatenate((pts, clr), -1)
    return pc


def segment_surface(scene_pc, surface_dist_threshold=0.02, abs_threshold=False):
    scene_o3d = to_o3d_pc(scene_pc.copy())

    plane_model, inliers = scene_o3d.segment_plane(
        distance_threshold=0.013, ransac_n=3, num_iterations=1000
    )
    [a, b, c, d] = plane_model
    print(f"Plane equation: {a:.2f}x + {b:.2f}y + {c:.2f}z + {d:.2f} = 0")

    inlier_cloud = scene_o3d.select_by_index(inliers)
    # inlier_cloud.paint_uniform_color([1.0, 0, 0])
    outlier_cloud = scene_o3d.select_by_index(inliers, invert=True)
    surface_o3d = inlier_cloud
    surface_pc = o3d_to_np(surface_o3d)

    scene_o3d = outlier_cloud
    scene_pc = o3d_to_np(scene_o3d)
    dists = scene_pc[:, :3] @ plane_model[:3].reshape(-1, 1) + plane_model[3]
    if abs_threshold:
        dists = np.abs(dists)
    scene_pc = scene_pc[dists.reshape(-1) > surface_dist_threshold]

    return scene_pc, surface_pc, plane_model


def segment_scene(scene_pc, cam_vec=None, eps=0.03, min_pts=100, min_obj_h=0.05, verbose=False):
    scene_pc, surface_pc, surface_plane = segment_surface(scene_pc)
    objects = get_clusters(scene_pc, cam_vec, eps, min_pts, min_obj_h - surface_plane[-1], verbose)
    return scene_pc, surface_pc, objects


def get_clusters(scene_pc, cam_vec=None, eps=0.03, min_pts=100, min_obj_h=0.65, verbose=False):
    """
    scene_o3d: (np.array or o3d.geometry.PointCloud) point cloud
    Return:
    - objects: (list) objects with their poses, bounds, and point clouds
    """
    if isinstance(scene_pc, o3d.geometry.PointCloud):
        scene_o3d = scene_pc
        scene_pc = o3d_to_np(scene_o3d)
    else:
        scene_o3d = to_o3d_pc(scene_pc.copy())
    labels = np.array(scene_o3d.cluster_dbscan(eps=eps, min_points=min_pts, print_progress=False))
    max_label = labels.max()
    uniq_labels = np.arange(max_label + 1)
    if verbose:
        colors = plt.get_cmap("tab20")(labels / (max_label if max_label > 0 else 1))
        colors[labels < 0] = 0
        scene_o3d.colors = o3d.utility.Vector3dVector(colors[:, :3])
        o3d.visualization.draw_geometries(
            [scene_o3d],
        )
    seg_lengths = np.array([len(scene_pc[labels == lab]) for lab in uniq_labels])
    ordered_labels = uniq_labels[np.argsort(seg_lengths)[::-1]]
    objects = []
    for lab in ordered_labels:
        pc = scene_pc[labels == lab]
        # only keep clusters that contain points at least 4cm above the table
        if pc[:, 2].max() > min_obj_h:
            pc_pose, pc_bounds = get_pc_pose(pc, cam_vec, include_bounds=True)
            objects.append(
                (
                    pc_pose,
                    pc_bounds,
                    pc,
                )
            )
    print(f"point cloud has {len(objects)} clusters")
    return objects


def cut_pc_boundaries(pc, extents):
    # Only look at stuff near the table
    xkeeps = np.logical_and(
        pc[:, 0] < extents[1][0],
        pc[:, 0] > extents[0][0],
    )
    ykeeps = np.logical_and(
        pc[:, 1] < extents[1][1] + 0.1,
        pc[:, 1] > extents[0][1] - 0.1,
    )
    zkeeps = np.logical_and(
        pc[:, 2] > extents[0][2],
        pc[:, 2] < extents[1][2],
    )
    keepidx = np.logical_and(
        np.logical_and(
            xkeeps,
            ykeeps,
        ),
        zkeeps,
    )
    return pc[keepidx]
